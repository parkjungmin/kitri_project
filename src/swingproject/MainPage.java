package swingproject;

import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Graphics;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.SwingConstants;

import swingproject.view.JoinFrame;
import swingproject.view.LoginFrame;

import java.awt.Color;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.ActionEvent;

public class MainPage extends JFrame {
	JScrollPane scrollPane;

	// 멤버 필드에 ImageIcon 클래스 생성자
	private Image background = new ImageIcon(MainPage.class.getResource("./image/1234.jpg")).getImage();

	public MainPage() {

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(1000, 600);
		setVisible(true);
		setLocationRelativeTo(null);

		// 백그라운드 이미지 삽입할 메소드에 이름없는 클래스로 구현
		JPanel panel = new JPanel() {
			public void paintComponent(Graphics g) {
				// Approach 1: Dispaly image at at full size
//				g.drawImage(background, 0, 0, null);
				// Approach 2: Scale image to size of component

				Dimension d = getSize();
				g.drawImage(background, 0, 0, d.width, d.height, null);

				// Approach 3: Fix the image position in the scroll pane
				// Point p = scrollPane.getViewport().getViewPosition();
				// g.drawImage(icon.getImage(), p.x, p.y, null);
				setOpaque(false);
				super.paintComponent(g);
			}
		};
		// JButton button = new JButton("Hello");
		// panel.add(button);

		scrollPane = new JScrollPane(panel);
		panel.setLayout(null);

		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon(MainPage.class.getResource("/swingproject/image/logo3.png")));
		lblNewLabel.setForeground(new Color(245, 255, 250));
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setFont(new Font("Georgia", Font.BOLD | Font.ITALIC, 37));
		lblNewLabel.setBounds(228, 10, 450, 70);
		panel.add(lblNewLabel);
		
		JButton loginBtn = new JButton("Login");
		loginBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				LoginFrame lf = new LoginFrame();
				lf.addWindowListener(new WindowAdapter() {
					
					@Override
					public void windowClosed(WindowEvent e) {
						dispose();
					}
					
				});
			}
		});
		
		
		
		loginBtn.setBounds(719, 10, 120, 23);
		panel.add(loginBtn);
		
		JButton joinBtn = new JButton("Sign up");
		joinBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JoinFrame jf = new JoinFrame();
			}
		});
		joinBtn.setBounds(851, 10, 120, 23);
		panel.add(joinBtn);
		
		JLabel lblNewLabel_1 = new JLabel("\uB300\uD45C\uBC88\uD638");
		lblNewLabel_1.setBounds(12, 448, 102, 15);
		panel.add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("02)123-4567");
		lblNewLabel_2.setBounds(34, 469, 80, 15);
		panel.add(lblNewLabel_2);
		
		JLabel lblNewLabel_1_1 = new JLabel("E-MAIL");
		lblNewLabel_1_1.setBounds(12, 494, 102, 15);
		panel.add(lblNewLabel_1_1);
		
		JLabel lblNewLabel_2_1 = new JLabel("service@beautybox.com");
		lblNewLabel_2_1.setBounds(34, 509, 148, 15);
		panel.add(lblNewLabel_2_1);
		setContentPane(scrollPane);
		
		setVisible(true);
	}

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainPage mainpage = new MainPage();
					

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
}