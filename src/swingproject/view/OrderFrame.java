package swingproject.view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import swingproject.customer.customerManage.CustomerManagePage;
import swingproject.dao.HistoryDao;
import swingproject.dao.StockDao;
import swingproject.domain.History;
import swingproject.domain.Stock;

public class OrderFrame extends JFrame {

	private JPanel contentPane;
	private static String admin_id;
	private JTextField tfHistory_qty;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					OrderFrame frame = new OrderFrame(admin_id);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public OrderFrame(String admin_id) {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(650, 280, 413, 316);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		contentPane.setBackground(new Color(245, 255, 250));
		
		String cb[] = {"ĿƮ", "��", "����"};
		JComboBox<String> cbHistory_name;
		
		
		cbHistory_name = new JComboBox<String>(cb);
		cbHistory_name.setBounds(141, 72, 192, 30);
		contentPane.add(cbHistory_name);
		
		JLabel lblNewLabel2 = new JLabel("");
		lblNewLabel2.setIcon(new ImageIcon(LoginFrame.class.getResource("/swingproject/image/logo.png")));
		lblNewLabel2.setBounds(10, 0, 113, 79);
		contentPane.add(lblNewLabel2);
		
		JLabel lbTitle = new JLabel("\uBC1C\uC8FC \uC2E0\uCCAD");
		lbTitle.setPreferredSize(new Dimension(738, 50));
		lbTitle.setOpaque(true);
		lbTitle.setHorizontalAlignment(SwingConstants.CENTER);
		lbTitle.setFont(new Font("Dialog", Font.BOLD, 20));
		lbTitle.setBackground(new Color(245, 255, 250));
		lbTitle.setBounds(10, 1, 434, 50);
		contentPane.add(lbTitle);
		
		
		JLabel lblNewLabel = new JLabel("\uBC1C\uC8FC \uD488\uBAA9");
		lblNewLabel.setBounds(50, 80, 57, 15);
		contentPane.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("\uBC1C\uC8FC \uC218\uB7C9");
		lblNewLabel_1.setBounds(50, 130, 57, 15);
		contentPane.add(lblNewLabel_1);
		
		
		
		tfHistory_qty = new JTextField();
		tfHistory_qty.setBounds(141, 123, 192, 30);
		contentPane.add(tfHistory_qty);
		tfHistory_qty.setColumns(10);
		
		JButton okBtn = new JButton("\uD655\uC778");
		okBtn.setBounds(50, 202, 283, 30);
		contentPane.add(okBtn);
		
		okBtn.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				String combo = cbHistory_name.getSelectedItem().toString();
				History history = new History();
				Stock stock = new Stock();
//				history.setHistory_name(combo);
//				history.setHistory_qty(Integer.parseInt(tfHistory_qty.getText()));
				stock.setStock_name(combo);
				stock.setStock_come(Integer.parseInt(tfHistory_qty.getText()));

				StockDao sdao = StockDao.getInstance();
				HistoryDao hdao = HistoryDao.getInstance();
				
				int result = hdao.orderSave(stock);
				
				
				
				System.out.println(combo);
				System.out.println(stock);
				
				if(result == 1) {
					if(combo == "ĿƮ") {
						sdao.updateAStock(stock);
					
					JOptionPane.showMessageDialog(null, "��Ǫ ���� �Ϸ�");

					dispose();
					new StockFrame(admin_id);
					}else if(combo == "��") {
						sdao.updateBStock(stock);
						JOptionPane.showMessageDialog(null, "�ĸ��� ���� �Ϸ�");

						dispose();
						new StockFrame(admin_id);
					}else if(combo == "����") {
						sdao.updateCStock(stock);
						JOptionPane.showMessageDialog(null, "������ ���� �Ϸ�");
						dispose();
						new StockFrame(admin_id);
					}
				} else {
					JOptionPane.showMessageDialog(null, "���� ����");
					dispose();
				}
			}
		});
		
		setVisible(true);
	}
}
