package swingproject.view;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import swingproject.dao.CalendarBooking;
import swingproject.dao.MemberDAO;

public class LoginFrame extends JFrame {

	private JPanel contentPane;
	private JTextField tfCustomer_id;
	private JTextField tfCustomer_pw;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					LoginFrame frame = new LoginFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public LoginFrame() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(500,500);
		setBounds(700, 250, 500, 500);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(245, 255, 250));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		//id
				tfCustomer_id = new JTextField();
				tfCustomer_id.setBounds(169, 170, 225, 32);
				contentPane.add(tfCustomer_id);
				tfCustomer_id.setColumns(10);
				
				
				//패스워드
				tfCustomer_pw = new JPasswordField();
				tfCustomer_pw.setBounds(169, 236, 225, 32);
				contentPane.add(tfCustomer_pw);
				tfCustomer_pw.setColumns(10);
				
				JLabel lblNewLabel = new JLabel("id");
				lblNewLabel.setFont(new Font("굴림", Font.BOLD, 12));
				lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
				lblNewLabel.setBounds(87, 169, 57, 32);
				contentPane.add(lblNewLabel);
				
				JLabel lblNewLabel_1 = new JLabel("pw");
				lblNewLabel_1.setFont(new Font("굴림", Font.BOLD, 12));
				lblNewLabel_1.setHorizontalAlignment(SwingConstants.CENTER);
				lblNewLabel_1.setBounds(87, 235, 57, 32);
				contentPane.add(lblNewLabel_1);
				
				String loginimgPath = "C:\\Users\\Administrator\\Desktop\\swing image\\login2.jpg";
				ImageIcon originicon = new ImageIcon(loginimgPath);
				Image originImg = originicon.getImage();
				Image changedImg = originImg.getScaledInstance(300, 200, Image.SCALE_SMOOTH);
				
				JButton loginBtn = new JButton();
				loginBtn.setIcon(null);
				loginBtn.setOpaque(true);
				loginBtn.setBackground(new Color(192, 192, 192));
				loginBtn.setFont(new Font("굴림", Font.BOLD, 12));
				loginBtn.setText("\uB85C\uADF8\uC778");
				loginBtn.setBounds(87, 314, 143, 32);
				contentPane.add(loginBtn);
				
				JButton joinBtn = new JButton("\uD68C\uC6D0\uAC00\uC785");
				joinBtn.setFont(new Font("굴림", Font.BOLD, 12));
				joinBtn.setOpaque(true);
				joinBtn.setBackground(new Color(192, 192, 192));
				joinBtn.setBounds(251, 314, 143, 32);
				contentPane.add(joinBtn);
				
				JButton adminloginBtn = new JButton();
				adminloginBtn.setText("\uAD00\uB9AC\uC790 \uB85C\uADF8\uC778");
				adminloginBtn.setOpaque(true);
				adminloginBtn.setFont(new Font("굴림", Font.BOLD, 12));
				adminloginBtn.setBackground(Color.LIGHT_GRAY);
				adminloginBtn.setBounds(87, 371, 307, 32);
				contentPane.add(adminloginBtn);
				
				JLabel lblNewLabel_3 = new JLabel(new ImageIcon(LoginFrame.class.getResource("/swingproject/image/logo2.png")));
				lblNewLabel_3.setBounds(116, 25, 248, 135);
				contentPane.add(lblNewLabel_3);
				
				setVisible(true);
				//로그인 액션
//				loginBtn.addActionListener(new ActionListener() {
		//
//					@Override
//					public void actionPerformed(ActionEvent e) {
//						// TODO Auto-generated method stub
//						MemberListFrame frame = new MemberListFrame(customer_id);
//					}
//					
//				});
				
				//회원가입액션
				joinBtn.addActionListener(new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent e) {
						JoinFrame frame = new JoinFrame();
						
					}
				});
				
				loginBtn.addActionListener(new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent e) {
						String customer_id = tfCustomer_id.getText();
						String customer_pw = tfCustomer_pw.getText();
						MemberDAO dao = MemberDAO.getInstance();
						int result = dao.findByUserNameAndPassword(customer_id, customer_pw);
						System.out.println(result);
						if(result == 1) {
							JOptionPane.showMessageDialog(null, "로그인 성공");
							//리스트화면으로 + 세션값
							
							CalendarBooking frame = new CalendarBooking(customer_id);
							
							dispose();
							System.out.println("로그인 세션 customer_id = " + customer_id);
						} else {
							JOptionPane.showMessageDialog(null, "아이디 비밀번호를 확인해주세요");
						}
						
					}
				});
				
				adminloginBtn.addActionListener(new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent e) {
						String admin_id = tfCustomer_id.getText();
						String admin_pw = tfCustomer_pw.getText();
						MemberDAO mdao = MemberDAO.getInstance();
						int result = mdao.findAdminUserNameAndPassword(admin_id, admin_pw);
						System.out.println(result);
						if(result == 1) {
							JOptionPane.showMessageDialog(null, "관리자 로그인 성공");
							
							BoardListFrame bf = new BoardListFrame(admin_id);
							
							dispose();
							System.out.println("관리자 로그인 세션 admin_id = " + admin_id);
						} else {
							JOptionPane.showMessageDialog(null, "아이디 비밀번호를 확인해주세요");
						}
					}
				});
			}
		}
