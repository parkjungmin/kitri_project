package swingproject.view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;


import swingproject.customer.customerManage.CustomerManagePage;
import swingproject.dao.BookNow;
import swingproject.dao.HistoryDao;
import swingproject.dao.StockDao;
import swingproject.domain.History;
import swingproject.domain.Stock;
import javax.swing.Icon;
import javax.swing.ImageIcon;

public class StockFrame extends JFrame {

	private JPanel contentPane;
	private String colNames[] = { "board_no", "board_name", "board_content", "board_date", "board_count" };
	private DefaultTableModel tableModel = new DefaultTableModel(colNames, 0);
	private JTable table;
	private String colNames1[] = { "board_no", "board_name", "board_content", "board_date", "board_count" };
	private DefaultTableModel tableModel1 = new DefaultTableModel(colNames1, 0);
	private JTable table1;
	private static String admin_id;

	/**
	 * Launch the application.
	 */


	/**
	 * Create the frame.
	 */
	public StockFrame(String admin_id) {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(1000, 600);
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(140, 104, 809, 115);
		setLocationRelativeTo(null);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(245, 255, 250));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton logoutBtn = new JButton("\uB85C\uADF8\uC544\uC6C3");
		logoutBtn.setBounds(861, 38, 88, 23);
		contentPane.add(logoutBtn);
		logoutBtn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(null, "로그아웃");
				dispose();
				new LoginFrame();

			}
		});
		
		JLabel adminid = new JLabel(admin_id + "님 환영합니다");
		adminid.setBounds(725, 42, 164, 15);
		contentPane.add(adminid);

		
		

		JLabel lbTitle = new JLabel("재고 현황");
		lbTitle.setOpaque(true);
		lbTitle.setBackground(new Color(245, 255, 250));
		lbTitle.setBounds(230, 0, 655, 94);
		lbTitle.setHorizontalAlignment(SwingConstants.CENTER);
		lbTitle.setFont(new Font("나눔고딕", Font.BOLD, 20));
		lbTitle.setPreferredSize(new Dimension(738, 50));
		contentPane.add(lbTitle);

		StockDao stockdao = StockDao.getInstance();
		List<Stock> list = stockdao.readAll();

		String[] colNames = new String[] { "재고 번호", "이름", "수량", "가격" };
		Object[][] rowDatas = new Object[list.size()][colNames.length];

		for (int i = 0; i < list.size(); i++) {
			rowDatas[i] = new Object[] { list.get(i).getStock_id(), list.get(i).getStock_name(),
					list.get(i).getStock_qty(), list.get(i).getStock_price() };
		}

		table = new JTable(tableModel);
		stockdao.readAll();
		table.setModel(new DefaultTableModel(rowDatas, colNames) {
			boolean[] columnEditables = new boolean[] { false, false, false, false };

			public boolean isCellEditable(int row, int column) {
				return columnEditables[column];
			}

		});
		table.getColumnModel().getColumn(0).setResizable(false);
		table.getColumnModel().getColumn(0).setPreferredWidth(45);
		table.getColumnModel().getColumn(1).setResizable(false);
		table.getColumnModel().getColumn(1).setPreferredWidth(200);
		table.getColumnModel().getColumn(2).setResizable(false);
		table.getColumnModel().getColumn(2).setPreferredWidth(164);
		table.getColumnModel().getColumn(3).setResizable(false);
		table.getColumnModel().getColumn(3).setPreferredWidth(164);
		scrollPane.setViewportView(table);
		
		table.setFont(new Font("돋움", Font.PLAIN, 20));
		table.setRowHeight(30);
		
		contentPane.add(scrollPane);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(141, 308, 808, 210);
		contentPane.add(scrollPane_1);
		
		
		HistoryDao historydao = HistoryDao.getInstance();
//		List<History> list1 = historydao.readAll();
//		StockDao stockdao1 = StockDao.getInstance();
		List<Stock> list1 = historydao.readAll();

		String[] colNames1 = new String[] { "주문 번호", "이름", "날짜", "수량" };
		Object[][] rowDatas1 = new Object[list1.size()][colNames1.length];

//		for (int i = 0; i < list1.size(); i++) {
//			rowDatas1[i] = new Object[] { list1.get(i).getHistory_no(), 
//					list1.get(i).getHistory_name(), list1.get(i).getHistory_date(),
//					list1.get(i).getHistory_qty()};
//		}
		
		for (int i = 0; i < list1.size(); i++) {
			rowDatas1[i] = new Object[] { list1.get(i).getStock_id(), 
					list1.get(i).getStock_name(), list1.get(i).getStock_date(),
					list1.get(i).getStock_come()};
		}
		
		table1 = new JTable(tableModel);
		historydao.readAll();
		table1.setModel(new DefaultTableModel(rowDatas1, colNames1) {
			boolean[] columnEditables = new boolean[] { false, false, false, false };

			public boolean isCellEditable(int row, int column) {
				return columnEditables[column];
			}

		});
//		System.out.println(list1.get(1).getHistory_no());
		table1.getColumnModel().getColumn(0).setResizable(false);
		table1.getColumnModel().getColumn(0).setPreferredWidth(45);
		table1.getColumnModel().getColumn(1).setResizable(false);
		table1.getColumnModel().getColumn(1).setPreferredWidth(200);
		table1.getColumnModel().getColumn(2).setResizable(false);
		table1.getColumnModel().getColumn(2).setPreferredWidth(164);
		table1.getColumnModel().getColumn(3).setResizable(false);
		table1.getColumnModel().getColumn(3).setPreferredWidth(164);
		scrollPane_1.setViewportView(table1);
		
		table1.setFont(new Font("돋움", Font.PLAIN, 20));
		table1.setRowHeight(30);
		
		contentPane.add(scrollPane_1);

		
		
		
		
		
		
		JButton boardBtn = new JButton("\uACF5\uC9C0\uC0AC\uD56D");
		boardBtn.setBounds(10, 104, 118, 75);
		contentPane.add(boardBtn);
		
		JButton customerBtn = new JButton("\uACE0\uAC1D\uAD00\uB9AC");
		customerBtn.setBounds(12, 189, 116, 75);
		contentPane.add(customerBtn);
		
		JButton bookingBtn = new JButton("\uC608\uC57D\uAD00\uB9AC");
		bookingBtn.setBounds(10, 274, 118, 75);
		contentPane.add(bookingBtn);
		
		JButton stockBtn = new JButton("\uC7AC\uACE0\uAD00\uB9AC");
		stockBtn.setBounds(10, 359, 118, 75);
		contentPane.add(stockBtn);
		
		JLabel lbTitle_1 = new JLabel("\uC785\uACE0 \uD604\uD669");
		lbTitle_1.setPreferredSize(new Dimension(738, 50));
		lbTitle_1.setOpaque(true);
		lbTitle_1.setHorizontalAlignment(SwingConstants.CENTER);
		lbTitle_1.setFont(new Font("Dialog", Font.BOLD, 20));
		lbTitle_1.setBackground(new Color(245, 255, 250));
		lbTitle_1.setBounds(140, 248, 843, 50);
		contentPane.add(lbTitle_1);
		
		JButton export = new JButton("csv파일 변환");
		export.setBounds(831, 528, 118, 30);
		contentPane.add(export);
		

		
		
		export.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				Date sysdate = new Date();
				SimpleDateFormat format;
				format = new SimpleDateFormat("yyyy년MM월dd일HH시mm분");
				String filePath = "C:\\Users\\Administrator\\Desktop\\csv파일\\재고관리" + format.format(sysdate) + ".csv";
				File file = new File(filePath);
				
				try {
					FileWriter fw = new FileWriter(file);
					BufferedWriter bw = new BufferedWriter(fw);
					
					//컬럼명
					
					bw.write("재고 현황");
					bw.newLine();
					for(int k = 0; k < table.getColumnCount(); k++) {
						bw.write(table.getColumnName(k) + ",");
						
					}

					bw.newLine();
					//셀 내용 채우기
					for(int i = 0; i < table.getRowCount(); i++) {
						for(int j = 0; j <table.getColumnCount(); j++) {
							bw.write(table.getValueAt(i, j).toString());
							bw.write(",");
						}
						bw.newLine();
						
					}
					
					bw.newLine();
					bw.write("입고 현황");
					bw.newLine();
					for(int l = 0; l <table1.getColumnCount(); l++) {
						bw.write(table1.getColumnName(l) + ",");
					}
					bw.newLine();
					for(int i = 0; i < table1.getRowCount(); i++) {
						for(int j = 0; j <table1.getColumnCount(); j++) {
							bw.write(table1.getValueAt(i, j).toString());
							bw.write(",");
						}
						bw.newLine();
						
					}
					
					
					bw.close();
					fw.close();
					System.out.println(table.getRowCount());
					System.out.println(table.getSelectedColumn());
					System.out.println(table.getSelectedRow());
					System.out.println(table.getColumnCount());
					JOptionPane.showMessageDialog(null, "변환 성공");
					System.out.println("ggg");
				} catch (IOException ex) {
					ex.printStackTrace();
					JOptionPane.showMessageDialog(null, "변환 실패");
					System.out.println("변환 실패");
				}
				
			}
		});
		
		JButton orderBtn = new JButton("\uBC1C\uC8FC");
		orderBtn.setBounds(671, 528, 118, 30);
		contentPane.add(orderBtn);
		
		JLabel lblNewLabel = new JLabel(new ImageIcon(StockFrame.class.getResource("/swingproject/image/logo2.png")));
		lblNewLabel.setBounds(12, 10, 164, 84);
		contentPane.add(lblNewLabel);
		
		
		
		



		
		orderBtn.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				dispose();
				new OrderFrame(admin_id);
				
			}
		});
		

		setVisible(true);
		boardBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new BoardListFrame(admin_id);
				dispose();
			}
		});
		customerBtn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				dispose();
				CustomerManagePage cm = new CustomerManagePage(admin_id);

			}
		});
		bookingBtn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				dispose();
				new BookNow(admin_id);

			}
		});
		
		stockBtn.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				dispose();
				StockFrame sf = new StockFrame(admin_id);
				
			}
		});
		
	}
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					StockFrame frame = new StockFrame(admin_id);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
}
