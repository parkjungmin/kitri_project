package swingproject.view;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import swingproject.dao.MemberDAO;
import swingproject.domain.Member;
import javax.swing.JPasswordField;
import java.awt.Color;
import javax.swing.SwingConstants;
import javax.swing.ImageIcon;

public class JoinFrame extends JFrame {

	public static JButton bt_checkId;
	private JPanel contentPane;
	private static JTextField tfCustomer_id;
	private JTextField tfCustomer_pw;
	private JTextField tfCustomer_name;
	private JTextField tfCustomer_phone;
	private JTextField tfCustomer_birth;
	private static String customer_id;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					JoinFrame frame = new JoinFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public JoinFrame() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(700, 250, 500, 500);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(245, 255, 250));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel_7 = new JLabel("");
		lblNewLabel_7.setIcon(new ImageIcon(JoinFrame.class.getResource("/swingproject/image/logo.png")));
		lblNewLabel_7.setBounds(25, 10, 124, 69);
		contentPane.add(lblNewLabel_7);

		JLabel lblNewLabel = new JLabel("\uD68C\uC6D0\uAC00\uC785");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setOpaque(true);
		lblNewLabel.setBackground(new Color(245, 255, 250));
		lblNewLabel.setFont(new Font("굴림", Font.BOLD, 30));
		lblNewLabel.setBounds(0, 0, 484, 79);
		contentPane.add(lblNewLabel);

		tfCustomer_id = new JTextField();
		tfCustomer_id.setBounds(156, 100, 200, 21);
		contentPane.add(tfCustomer_id);
		tfCustomer_id.setColumns(10);

		tfCustomer_pw = new JPasswordField();
		tfCustomer_pw.setBounds(156, 140, 200, 21);
		contentPane.add(tfCustomer_pw);
		tfCustomer_pw.setColumns(10);

		tfCustomer_name = new JTextField();
		tfCustomer_name.setColumns(10);
		tfCustomer_name.setBounds(156, 180, 200, 21);
		contentPane.add(tfCustomer_name);

		tfCustomer_phone = new JTextField();
		tfCustomer_phone.setColumns(10);
		tfCustomer_phone.setBounds(156, 220, 200, 21);
		contentPane.add(tfCustomer_phone);

		tfCustomer_birth = new JTextField();
		tfCustomer_birth.setColumns(10);
		tfCustomer_birth.setBounds(156, 260, 200, 21);
		contentPane.add(tfCustomer_birth);

		JLabel lblNewLabel_1 = new JLabel("id");
		lblNewLabel_1.setBounds(60, 103, 57, 15);
		contentPane.add(lblNewLabel_1);

		JLabel lblNewLabel_2 = new JLabel("password");
		lblNewLabel_2.setBounds(60, 143, 57, 15);
		contentPane.add(lblNewLabel_2);

		JLabel lblNewLabel_3 = new JLabel("\uC774\uB984");
		lblNewLabel_3.setBounds(60, 183, 57, 15);
		contentPane.add(lblNewLabel_3);

		JLabel lblNewLabel_4 = new JLabel("\uC804\uD654\uBC88\uD638");
		lblNewLabel_4.setBounds(60, 223, 57, 15);
		contentPane.add(lblNewLabel_4);

		JLabel lblNewLabel_5 = new JLabel("\uC0DD\uB144\uC6D4\uC77C");
		lblNewLabel_5.setBounds(60, 263, 57, 15);
		contentPane.add(lblNewLabel_5);

		JTextArea taCustomer_address = new JTextArea();
		taCustomer_address.setBounds(156, 312, 200, 57);
		contentPane.add(taCustomer_address);

		JLabel lblNewLabel_6 = new JLabel("\uC8FC\uC18C");
		lblNewLabel_6.setBounds(60, 317, 57, 15);
		contentPane.add(lblNewLabel_6);

		JButton joinCompleteBtn = new JButton("회원가입");
		joinCompleteBtn.setBackground(Color.LIGHT_GRAY);
		joinCompleteBtn.setBounds(156, 396, 200, 55);
		contentPane.add(joinCompleteBtn);

		JButton bt_checkId = new JButton("\uC911\uBCF5\uD655\uC778");
		bt_checkId.setBackground(new Color(192, 192, 192));
		bt_checkId.setBounds(375, 99, 97, 23);
		contentPane.add(bt_checkId);

		setVisible(true);
		MemberDAO dao = new MemberDAO();
		boolean duplication = dao.findExistId(customer_id);
		System.out.println(duplication + "밖");
		
		joinCompleteBtn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
//				JOptionPane.showMessageDialog(null, "확인");
//				dispose();
				Member member = new Member();
				member.setCustomer_id(tfCustomer_id.getText());
				member.setCustomer_pw(tfCustomer_pw.getText());
				member.setCustomer_name(tfCustomer_name.getText());
				member.setCustomer_phone(tfCustomer_phone.getText());
				member.setCustomer_birth(tfCustomer_birth.getText());
				member.setCustomer_address(taCustomer_address.getText());

				MemberDAO dao = MemberDAO.getInstance();
				int result = dao.save(member);
				if(tfCustomer_id.getText().length() == 0 && tfCustomer_id.getText().isEmpty()) {
					JOptionPane.showMessageDialog(null, "null값 존재 id를 입력하세요");
				} else if(tfCustomer_pw.getText().length() == 0 && tfCustomer_pw.getText().isEmpty()) {
					JOptionPane.showMessageDialog(null, "null값 존재 비밀번호를 입력하세요");
				} else if(tfCustomer_name.getText().length() == 0 && tfCustomer_name.getText().isEmpty()) {
					JOptionPane.showMessageDialog(null, "null값 존재 이름를 입력하세요");
				} else if(tfCustomer_phone.getText().length() == 0 && tfCustomer_phone.getText().isEmpty()) {
					JOptionPane.showMessageDialog(null, "null값 존재 번호를 입력하세요");
				} else if(tfCustomer_birth.getText().length() == 0 && tfCustomer_birth.getText().isEmpty()) {
					JOptionPane.showMessageDialog(null, "null값 존재 생년월일를 입력하세요");
				} else if(taCustomer_address.getText().length() == 0 && taCustomer_address.getText().isEmpty()) {
					JOptionPane.showMessageDialog(null, "null값 존재 주소를 입력하세요");
				} else {
					
				
					
					if (result == 1) {
						JOptionPane.showMessageDialog(null, "회원가입 완료");
						LoginFrame frame = new LoginFrame();
						dispose();

					} else {
						JOptionPane.showMessageDialog(null, "회원가입 실패");
						dispose();
					}
				
				}

			}

		});

//		bt_checkId.addActionListener(new ActionListener() {
//			
//			@Override
//			public void actionPerformed(ActionEvent e) {
//				MemberDAO dao = MemberDAO.getInstance();
//				Object ob = e.getSource();
//				if(ob==JoinFrame.bt_checkId) {
//					if(dao.findExistId(JoinFrame.tfCustomer_id.getText())) {
//						JOptionPane.showMessageDialog(null, "사용중인 아이디");
//						JoinFrame.tfCustomer_id.setText("");
//						return;
//					} else {
//						JOptionPane.showMessageDialog(null, "사용 가능합니다");
//					}
//				}
//				
//			}
//		});

		bt_checkId.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				MemberDAO dao = MemberDAO.getInstance();
				Object ob = e.getSource();
				if (tfCustomer_id.getText().length() > 0) {
					if (dao.findExistId(JoinFrame.tfCustomer_id.getText())) {
						JOptionPane.showMessageDialog(null, "사용중인 아이디입니다");
						JoinFrame.tfCustomer_id.setText("");
						System.out.println(dao.findExistId(JoinFrame.tfCustomer_id.getText()) + "안");
						return;
					} else {
						JOptionPane.showMessageDialog(null, "사용 가능한 아이디입니다");
						System.out.println(dao.findExistId(JoinFrame.tfCustomer_id.getText()) + " 안 else");
					}
				}

			}
		});
	}
}
