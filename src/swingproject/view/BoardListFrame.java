package swingproject.view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import swingproject.customer.customerManage.CustomerManagePage;
import swingproject.dao.BoardDao;
import swingproject.dao.BookNow;
import swingproject.domain.Board;
import javax.swing.ImageIcon;

public class BoardListFrame extends JFrame {

	private static String admin_id; // 세션
	private static String customer_id;
	private JPanel contentPane;
	private JTable table;
	private JLabel lbTitle;
	private JButton logoutBtn;
	private String colNames[] = { "board_no", "board_name", "board_content", "board_date", "board_count" };
	private DefaultTableModel tableModel = new DefaultTableModel(colNames, 0);;
	private int board_no;

	private String driver = "oracle.jdbc.driver.OracleDriver";
	private String url = "jdbc:oracle:thin:@192.168.0.24:1521:xe";// @호스트 IP : 포트 : SID
	private String USER_NAME = "c##ora_user";
	private String USER_PW = "123";

	private Connection con = null;
	private PreparedStatement pstmt = null;
	private ResultSet rs = null;
	private String str;

	/**
	 * Launch the application.
	 */

	/**
	 * Create the frame.
	 */
//	public BoardListFrame() {
//		this(null, int board_no);
//	}

	public BoardListFrame(String admin_id) {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(1000, 600);
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(140, 104, 809, 408);
		setLocationRelativeTo(null);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(245, 255, 250));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		System.out.println("BoardListFrame 세션 id: " + admin_id);
		contentPane.setLayout(null);
		
		JButton logoutBtn = new JButton("\uB85C\uADF8\uC544\uC6C3");
		logoutBtn.setBounds(861, 39, 88, 23);
		contentPane.add(logoutBtn);
		
		JLabel adminid = new JLabel(admin_id + "님 환영합니다");
		adminid.setBounds(725, 43, 164, 15);
		contentPane.add(adminid);
		
		JLabel lblNewLabel = new JLabel("New label");
		lblNewLabel.setIcon(new ImageIcon(LoginFrame.class.getResource("/swingproject/image/logo2.png")));
		lblNewLabel.setBounds(0, 10, 180, 84);
		contentPane.add(lblNewLabel);
		JLabel lbTitle = new JLabel("\uACF5\uC9C0\uC0AC\uD56D");
		lbTitle.setOpaque(true);
		lbTitle.setBackground(new Color(245, 255, 250));
		lbTitle.setBounds(0, 0, 984, 94);
		lbTitle.setHorizontalAlignment(SwingConstants.CENTER);
		lbTitle.setFont(new Font("나눔고딕", Font.BOLD, 20));
		lbTitle.setPreferredSize(new Dimension(738, 50));
		contentPane.add(lbTitle);
		// db데이터 가져오기

		BoardDao dao = BoardDao.getInstance();
		List<Board> list = dao.readAll();

		// tableModel에 열 이름과 행 개수 설정
//		tableModel = new DefaultTableModel(boardName,0);
		String[] colNames = new String[] { "번호", "제목", "작성날짜", "작성자" };
		Object[][] rowDatas = new Object[list.size()][colNames.length];
//		// tableModel에 전체 행 넣기
//
		for (int i = 0; i < list.size(); i++) {

			rowDatas[i] = new Object[] { list.get(i).getBoard_no(), list.get(i).getBoard_name(), list.get(i).getBoard_date(), list.get(i).getAdmin_id()

			};
		}

		// tableModel을 JTable에 넣기
		table = new JTable(tableModel);
		dao.readAll();
		System.out.println(dao.readAll());
		table.setModel(new DefaultTableModel(rowDatas, colNames) {
			boolean[] columnEditables = new boolean[] { false, false,  false, false };

			public boolean isCellEditable(int row, int column) {
				return columnEditables[column];
			}

		});
		table.getColumnModel().getColumn(0).setResizable(false);
		table.getColumnModel().getColumn(0).setPreferredWidth(45);
		table.getColumnModel().getColumn(1).setResizable(false);
		table.getColumnModel().getColumn(1).setPreferredWidth(200);
		table.getColumnModel().getColumn(2).setResizable(false);
		table.getColumnModel().getColumn(2).setPreferredWidth(164);
		table.getColumnModel().getColumn(3).setResizable(false);
		table.getColumnModel().getColumn(3).setPreferredWidth(140);
		table.addMouseListener(new JTableMouseListener());
//        
//        
//        table.addMouseListener(new MouseAdapter() {
//        	
//        	@Override
//        	public void mouseClicked(MouseEvent e) {
//        		int rowNum = table.getSelectedRow();
//        		Board board = new Board();
//        		BoardDao dao = new BoardDao();
//        		board = list.get(rowNum);
//        		System.out.println("=================");
//        		System.out.println("선택된 보드 넘버 " + rowNum);
//        		System.out.println("================");
//        		new BoardDetailFrame(board);
//        	}
//        });

		scrollPane.setViewportView(table);

		JButton boardDeleteBtn = new JButton("\uAE00 \uC0AD\uC81C");
		boardDeleteBtn.setBounds(809, 522, 140, 29);
		contentPane.add(boardDeleteBtn);
		boardDeleteBtn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				System.out.println(e.getActionCommand()); // 선택된 버튼의 텍스트값 출력
				DefaultTableModel model2 = (DefaultTableModel) table.getModel();
				int row = table.getSelectedRow();
				if (row < 0)
					return; // 선택이 안된 상태면 -1리턴
				String query = "DELETE FROM board WHERE board_no = ?";

				try {
					Class.forName(driver); // 드라이버 로딩
					con = DriverManager.getConnection(url, USER_NAME, USER_PW);
					pstmt = con.prepareStatement(query);

					// 물음표가 1개 이므로 4개 각각 입력해줘야한다.
					pstmt.setString(1, String.valueOf(model2.getValueAt(row, 0)));
					int cnt = pstmt.executeUpdate();
					// pstmt.executeUpdate(); create insert update delete
					// pstmt.executeQuery(); select
				} catch (Exception eeee) {
					System.out.println(eeee.getMessage());
				} finally {
					try {
						pstmt.close();
						con.close();
					} catch (Exception e2) {
					}
				}
				model2.removeRow(row); // 테이블 상의 한줄 삭제

			}
		});
		getContentPane().add(boardDeleteBtn);

		scrollPane.setViewportView(table);

		table.setFont(new Font("돋움", Font.PLAIN, 20));
		table.setRowHeight(30);

		// JTable에 scroll달아서 add하기
//		JScrollPane scrollPane = new JScrollPane(table);
		contentPane.add(scrollPane);



		JButton boardCreateBtn = new JButton("\uAE00 \uC791\uC131");
		boardCreateBtn.setBounds(657, 521, 140, 30);
		contentPane.add(boardCreateBtn);

		JButton boardBtn = new JButton("\uACF5\uC9C0\uC0AC\uD56D");
		boardBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new BoardListFrame(admin_id);
				dispose();
			}
		});
		boardBtn.setBounds(10, 104, 118, 75);
		contentPane.add(boardBtn);

		JButton customerBtn = new JButton("\uACE0\uAC1D\uAD00\uB9AC");
		customerBtn.setBounds(12, 189, 116, 75);
		contentPane.add(customerBtn);

		JButton stockBtn = new JButton("\uC7AC\uACE0\uAD00\uB9AC");
		stockBtn.setBounds(10, 359, 118, 75);
		contentPane.add(stockBtn);

		JButton bookingBtn = new JButton("\uC608\uC57D\uAD00\uB9AC");
		bookingBtn.setBounds(10, 274, 118, 75);
		contentPane.add(bookingBtn);

		bookingBtn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				dispose();
				new BookNow(admin_id);

			}
		});

		setVisible(true);

		logoutBtn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(null, "로그아웃");
				dispose();
				new LoginFrame();

			}
		});
		boardCreateBtn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				BoardCreateFrame bcfame = new BoardCreateFrame(admin_id);
				dispose();
//				new BoardCreateFrame();

			}

		});

		customerBtn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				dispose();
				CustomerManagePage cm = new CustomerManagePage(admin_id);

			}
		});
		
		stockBtn.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				dispose();
				StockFrame sf = new StockFrame(admin_id);
				
			}
		});

//		if (customer_id == null) {
//			System.out.println("if문    " + customer_id);
//			JOptionPane.showMessageDialog(null, "인증되지 않은 사용자");
//			dispose();
//
//		} else {
//			setVisible(true);
//		}
	}

	private class JTableMouseListener implements MouseListener { // 마우스로 눌려진값확인하기
		public void mouseClicked(java.awt.event.MouseEvent e) { // 선택된 위치의 값을 출력
			Board board = new Board();
			BoardDao dao = new BoardDao();
			JTable table = (JTable) e.getSource();
			int row = table.getSelectedRow(); // 선택된 테이블의 행값
			String query = "select * from board where board_no = ?";
			board_no=(int)table.getValueAt(row, 0);
			try {
				Class.forName(driver); // 드라이버 로딩
				con = DriverManager.getConnection(url, USER_NAME, USER_PW);
				pstmt = con.prepareStatement(query);
				// 물음표가 1개 이므로 4개 각각 입력해줘야한다.
				pstmt.setInt(1, board_no);
				int cnt = pstmt.executeUpdate();
				// pstmt.executeUpdate(); create insert update delete
				 pstmt.executeQuery();
			} catch (Exception eeee) {
				System.out.println(eeee.getMessage());
			} finally {
				try {
					pstmt.close();
					con.close();
				} catch (Exception e2) {
				}
			}
			System.out.println(board_no);
			dao.updateCount(board);
			new BoardDetailFrame(board_no);

		}

		public void mouseEntered(java.awt.event.MouseEvent e) {
		}

		public void mouseExited(java.awt.event.MouseEvent e) {
		}

		public void mousePressed(java.awt.event.MouseEvent e) {
		}

		public void mouseReleased(java.awt.event.MouseEvent e) {
		}
	}

	public static void main(String[] args) {

		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
//					Member member = new Member();
					BoardListFrame frame = new BoardListFrame(admin_id);

//					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
}
