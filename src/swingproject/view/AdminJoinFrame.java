package swingproject.view;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import swingproject.dao.MemberDAO;
import swingproject.domain.Admin;

public class AdminJoinFrame extends JFrame {

	private JPanel contentPane;
	private static JTextField tfAdmin_id;
	private JTextField tfAdmin_pw;
	private JTextField tfAdmin_name;
	private JTextField tfAdmin_phone;
	private JTextField tfAdmin_birth;
	private static String admin_id;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AdminJoinFrame frame = new AdminJoinFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public AdminJoinFrame() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 500, 500);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		contentPane.setLayout(null);

		JLabel lblNewLabel = new JLabel("\uD68C\uC6D0\uAC00\uC785");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setOpaque(true);
		lblNewLabel.setBackground(new Color(102, 205, 170));
		lblNewLabel.setFont(new Font("굴림", Font.BOLD, 30));
		lblNewLabel.setBounds(0, 0, 484, 79);
		contentPane.add(lblNewLabel);

		JLabel lblNewLabel_1 = new JLabel("id");
		lblNewLabel_1.setBounds(60, 103, 57, 15);
		contentPane.add(lblNewLabel_1);

		JLabel lblNewLabel_2 = new JLabel("password");
		lblNewLabel_2.setBounds(60, 143, 57, 15);
		contentPane.add(lblNewLabel_2);

		JLabel lblNewLabel_3 = new JLabel("\uC774\uB984");
		lblNewLabel_3.setBounds(60, 183, 57, 15);
		contentPane.add(lblNewLabel_3);

		JLabel lblNewLabel_4 = new JLabel("\uC804\uD654\uBC88\uD638");
		lblNewLabel_4.setBounds(60, 223, 57, 15);
		contentPane.add(lblNewLabel_4);

		JLabel lblNewLabel_5 = new JLabel("\uC0DD\uB144\uC6D4\uC77C");
		lblNewLabel_5.setBounds(60, 263, 57, 15);
		contentPane.add(lblNewLabel_5);

		tfAdmin_id = new JTextField();
		tfAdmin_id.setBounds(156, 100, 200, 21);
		contentPane.add(tfAdmin_id);
		tfAdmin_id.setColumns(10);

		tfAdmin_pw = new JPasswordField();
		tfAdmin_pw.setBounds(156, 140, 200, 21);
		contentPane.add(tfAdmin_pw);
		tfAdmin_pw.setColumns(10);

		tfAdmin_name = new JTextField();
		tfAdmin_name.setColumns(10);
		tfAdmin_name.setBounds(156, 180, 200, 21);
		contentPane.add(tfAdmin_name);

		tfAdmin_phone = new JTextField();
		tfAdmin_phone.setColumns(10);
		tfAdmin_phone.setBounds(156, 220, 200, 21);
		contentPane.add(tfAdmin_phone);

		tfAdmin_birth = new JTextField();
		tfAdmin_birth.setColumns(10);
		tfAdmin_birth.setBounds(156, 260, 200, 21);
		contentPane.add(tfAdmin_birth);

		MemberDAO mdao = new MemberDAO();

//		INT DUPLICATIONCHECK = MDAO.ADMINFINDEXISTID(ADMIN_ID);

//		JButton adminjoinCompleteBtn = new JButton("회원가입");
//		adminjoinCompleteBtn.setBackground(new Color(205, 133, 63));
//		adminjoinCompleteBtn.setBounds(156, 396, 200, 55);
//		contentPane.add(adminjoinCompleteBtn);

		JButton bt_admincheckId = new JButton("\uC911\uBCF5\uD655\uC778");
		bt_admincheckId.setBackground(new Color(192, 192, 192));
		bt_admincheckId.setBounds(375, 99, 97, 23);
		contentPane.add(bt_admincheckId);
		
		setVisible(true);
		
		JButton adminjoinCompleteBtn = new JButton("회원가입");
		adminjoinCompleteBtn.setBackground(new Color(205, 133, 63));
		adminjoinCompleteBtn.setBounds(156, 396, 200, 55);
		contentPane.add(adminjoinCompleteBtn);
		
		

		adminjoinCompleteBtn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				Admin admin = new Admin();
				admin.setAdmin_id(tfAdmin_id.getText());
				admin.setAdmin_pw(tfAdmin_pw.getText());
				admin.setAdmin_name(tfAdmin_name.getText());
				admin.setAdmin_phone(tfAdmin_phone.getText());
				admin.setAdmin_birth(tfAdmin_birth.getText());

				MemberDAO dao = MemberDAO.getInstance();
				int result = dao.adminSave(admin);

				if (tfAdmin_id.getText().length() == 0 && tfAdmin_id.getText().isEmpty()) {
					JOptionPane.showMessageDialog(null, "null값 존재 id를 입력하세요");
				} else if (tfAdmin_pw.getText().length() == 0 && tfAdmin_pw.getText().isEmpty()) {
					JOptionPane.showMessageDialog(null, "null값 존재 비밀번호를 입력하세요");
				} else if (tfAdmin_name.getText().length() == 0 && tfAdmin_name.getText().isEmpty()) {
					JOptionPane.showMessageDialog(null, "null값 존재 이름를 입력하세요");
				} else if (tfAdmin_phone.getText().length() == 0 && tfAdmin_phone.getText().isEmpty()) {
					JOptionPane.showMessageDialog(null, "null값 존재 번호를 입력하세요");
				} else if (tfAdmin_birth.getText().length() == 0 && tfAdmin_birth.getText().isEmpty()) {
					JOptionPane.showMessageDialog(null, "null값 존재 생년월일를 입력하세요");

				} else {

					if (result == 1) {
						JOptionPane.showMessageDialog(null, "관리자 가입 성공");
						LoginFrame frame = new LoginFrame();
						dispose();
					} else {
						JOptionPane.showMessageDialog(null, "가입 실패 다시 시도하세요");

					}
				}

			}
		});



		bt_admincheckId.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				MemberDAO dao = MemberDAO.getInstance();
				Object ob = e.getSource();
				if (tfAdmin_id.getText().length() > 0) {
					if (dao.adminfindExistId(AdminJoinFrame.tfAdmin_id.getText())) {
						JOptionPane.showMessageDialog(null, "사용중인 아이디입니다");
						AdminJoinFrame.tfAdmin_id.setText("");
						return;
					} else {
						JOptionPane.showMessageDialog(null, "사용 가능한 아이디입니다");
					}
				}

			}
		});
	}
}
