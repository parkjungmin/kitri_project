package swingproject.view;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import swingproject.dao.BoardDao;
import swingproject.domain.Board;
import java.awt.Color;
import javax.swing.ImageIcon;

public class BoardDetailFrame extends JFrame {


	private JPanel contentPane;
	private JTextField textField, tfBoard_name;
	private String driver = "oracle.jdbc.driver.OracleDriver";
	private String url = "jdbc:oracle:thin:@192.168.0.24:1521:xe";// @호스트 IP : 포트 : SID
	private String USER_NAME = "c##ora_user";
	private String USER_PW = "123";

	private Connection con = null;
	private PreparedStatement pstmt = null;
	private ResultSet rs = null;
	private String str;
	private String board_name=null;
	private String board_content=null;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			private int board_no;

			public void run() {
				try {
					BoardDetailFrame frame = new BoardDetailFrame(board_no);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public BoardDetailFrame(int board_no) {
		String query = "select board_name, board_content from board where board_no = ?";
		try {
			Class.forName(driver); // 드라이버 로딩
			con = DriverManager.getConnection(url, USER_NAME, USER_PW);
			pstmt = con.prepareStatement(query);
			// 물음표가 1개 이므로 4개 각각 입력해줘야한다.
			pstmt.setInt(1, board_no);
			int cnt = pstmt.executeUpdate();
			// pstmt.executeUpdate(); create insert update delete
			rs=pstmt.executeQuery();
			while(rs.next()) {
				board_name=rs.getString(1);
				board_content=rs.getString(2);
			}
		} catch (Exception eeee) {
			System.out.println(eeee.getMessage());
		} finally {
			try {
				pstmt.close();
				con.close();
				rs.close();
			} catch (Exception e2) {
			}
		}
		//new BoardDetailFrame(board_no);
		System.out.println(board_no);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 500, 400);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(245, 255, 250));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JTextArea taBoard_content = new JTextArea(board_content);
		taBoard_content.setBounds(52, 129, 373, 199);
		taBoard_content.setRows(5);
		contentPane.add(taBoard_content);

		JLabel lblNewLabel = new JLabel("New label");
		lblNewLabel.setIcon(new ImageIcon(BoardDetailFrame.class.getResource("/swingproject/image/logo.png")));
		lblNewLabel.setBounds(23, 0, 128, 121);
		contentPane.add(lblNewLabel);
		JLabel lblNewLabel_1 = new JLabel(board_name);
		lblNewLabel_1.setFont(new Font("굴림", Font.BOLD, 20));
		lblNewLabel_1.setBounds(163, 54, 193, 30);
		contentPane.add(lblNewLabel_1);
		
		setVisible(true);


	}
}
