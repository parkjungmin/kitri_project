package swingproject.domain;

import java.util.Date;

public class Stock {
	
	private int stock_id;
	private String stock_name;
	private int stock_qty;
	private int stock_price;
	private Date stock_date;
	private int stock_come;
	
	
	
	
	
	public Date getStock_date() {
		return stock_date;
	}
	public void setStock_date(Date stock_date) {
		this.stock_date = stock_date;
	}
	public int getStock_come() {
		return stock_come;
	}
	public void setStock_come(int stock_come) {
		this.stock_come = stock_come;
	}
	public int getStock_id() {
		return stock_id;
	}
	public void setStock_id(int stock_id) {
		this.stock_id = stock_id;
	}
	public String getStock_name() {
		return stock_name;
	}
	public void setStock_name(String stock_name) {
		this.stock_name = stock_name;
	}
	public int getStock_qty() {
		return stock_qty;
	}
	public void setStock_qty(int stock_qty) {
		this.stock_qty = stock_qty;
	}
	public int getStock_price() {
		return stock_price;
	}
	public void setStock_price(int stock_price) {
		this.stock_price = stock_price;
	}
	@Override
	public String toString() {
		return "Stock [stock_id=" + stock_id + ", stock_name=" + stock_name + ", stock_qty=" + stock_qty
				+ ", stock_price=" + stock_price + ", stock_date=" + stock_date + ", stock_come=" + stock_come + "]";
	}

	
	
	
}
