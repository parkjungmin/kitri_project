package swingproject.domain;

import java.util.Date;

public class History {

	private int history_stock_id;
	private String history_name;
	private Date history_date;
	private int history_qty;
	private int history_no;
	public int getHistory_stock_id() {
		return history_stock_id;
	}
	public void setHistory_stock_id(int history_stock_id) {
		this.history_stock_id = history_stock_id;
	}
	public String getHistory_name() {
		return history_name;
	}
	public void setHistory_name(String history_name) {
		this.history_name = history_name;
	}
	public Date getHistory_date() {
		return history_date;
	}
	public void setHistory_date(Date history_date) {
		this.history_date = history_date;
	}
	public int getHistory_qty() {
		return history_qty;
	}
	public void setHistory_qty(int history_qty) {
		this.history_qty = history_qty;
	}
	public int getHistory_no() {
		return history_no;
	}
	public void setHistory_no(int history_no) {
		this.history_no = history_no;
	}
	@Override
	public String toString() {
		return "History [history_stock_id=" + history_stock_id + ", history_name=" + history_name + ", history_date="
				+ history_date + ", history_qty=" + history_qty + ", history_no=" + history_no + "]";
	}

	
	
}
