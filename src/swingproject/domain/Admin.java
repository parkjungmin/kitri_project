package swingproject.domain;

public class Admin {
	
	private int admin_no;
	private String admin_id;
	private String admin_pw;
	private String admin_name;
	private String admin_phone;
	private String admin_birth;
	
	public int getAdmin_no() {
		return admin_no;
	}
	public void setAdmin_no(int admin_no) {
		this.admin_no = admin_no;
	}
	public String getAdmin_id() {
		return admin_id;
	}
	public void setAdmin_id(String admin_id) {
		this.admin_id = admin_id;
	}
	public String getAdmin_pw() {
		return admin_pw;
	}
	public void setAdmin_pw(String admin_pw) {
		this.admin_pw = admin_pw;
	}
	public String getAdmin_name() {
		return admin_name;
	}
	public void setAdmin_name(String admin_name) {
		this.admin_name = admin_name;
	}
	public String getAdmin_phone() {
		return admin_phone;
	}
	public void setAdmin_phone(String admin_phone) {
		this.admin_phone = admin_phone;
	}
	public String getAdmin_birth() {
		return admin_birth;
	}
	public void setAdmin_birth(String admin_birth) {
		this.admin_birth = admin_birth;
	}

	@Override
	public String toString() {
		return "Admin [admin_no=" + admin_no + ", admin_id=" + admin_id + ", admin_pw=" + admin_pw + ", admin_name=" + admin_name + ", admin_phone="
				+ admin_phone + ", admin_birth=" + admin_birth + "]";
	}
	
	
	
	
}	
