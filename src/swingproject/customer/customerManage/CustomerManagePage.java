package swingproject.customer.customerManage;

import java.awt.Button;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import swingproject.dao.BookNow;
import swingproject.dao.CalendarBooking;
import swingproject.view.BoardListFrame;
import swingproject.view.LoginFrame;
import swingproject.view.StockFrame;

import javax.swing.ImageIcon;

public class CustomerManagePage extends JFrame {
	
	private JPanel contentPane;
	private JPanel listPanel;
	private JTextField searchTextfield;
	private static String str ="";
	private static String customer_id;
	private static String admin_id;
	
	
	public CustomerManagePage(String admin_id) {
		init(admin_id);
		setVisible(true);
	}

	private void init(String admin_id) {
		
		// ������, contentpane ���� ����
		this.setBounds(100, 100, 1000, 600);
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setTitle("Customer Manage & List");
		
		
		
		contentPane = new JPanel();
		setContentPane(contentPane);
		contentPane.setLayout(null);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setBackground(new Color(245, 255, 250));
		
		JButton logoutBtn = new JButton("\uB85C\uADF8\uC544\uC6C3");
		logoutBtn.setBounds(861, 40, 88, 23);
		contentPane.add(logoutBtn);
		
		logoutBtn.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(null, "�α׾ƿ�");
				dispose();
				new LoginFrame();
				
			}
		});
		
		JLabel adminid = new JLabel(admin_id + "�� ȯ���մϴ�");
		adminid.setBounds(725, 44, 164, 15);
		contentPane.add(adminid);
		

		// mainLabel ����
		JLabel mainLabel  = new JLabel("���� ����");
		mainLabel.setOpaque(true);
		mainLabel.setBackground(new Color(245, 255, 250));
		mainLabel.setFont(new Font("��������", Font.BOLD, 20));
		mainLabel.setBounds(250, 0, 570, 94);
		mainLabel.setHorizontalAlignment(SwingConstants.CENTER);
		mainLabel.setPreferredSize(new Dimension(738, 50));

		contentPane.add(mainLabel);
		

		// listpanel ����
		listPanel = new CustomerList("");
		listPanel.setBounds(140, 137, 809, 414);
		contentPane.add(listPanel);
		
		// searchTextfield ����
		searchTextfield = new JTextField();
		searchTextfield.setHorizontalAlignment(SwingConstants.LEFT);
		searchTextfield.setForeground(Color.BLACK);
		searchTextfield.setBackground(Color.WHITE);
		searchTextfield.setBounds(430, 104, 277, 21);
		contentPane.add(searchTextfield);
		searchTextfield.setColumns(10);
		
		searchTextfield.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String str = searchTextfield.getText();
				setStr(str); 
				listPanel.setVisible(false);
				listPanel = new CustomerList(getStr());
				listPanel.setBounds(140, 137, 809, 414);
				contentPane.add(listPanel);
				listPanel.setVisible(true);

			}
		});

		// searchLabel ����
		JLabel searchLabel = new JLabel("Search : ");
		searchLabel.setFont(new Font("����", Font.ITALIC, 18));
		searchLabel.setBounds(350, 104, 80, 20);
		contentPane.add(searchLabel);
		
		
//		// menuBar ����
//		JMenuBar menuBar = new JMenuBar();
//		menuBar.setMargin(new Insets(100, 0, 100, 0));
//		menuBar.setFont(new Font("�ü�", Font.PLAIN, 15));
//
//		setJMenuBar(menuBar);
//		
//
//		
//		// menuBar button ����
//		Button menuBtn1 = new Button("\uACF5\uC9C0\uC0AC\uD56D");
//		menuBar.add(menuBtn1);
//		
//		menuBtn1.addActionListener(new ActionListener() {
//			public void actionPerformed(ActionEvent e) {
//
//				setVisible(false);
//				dispose();
//				BoardListFrame blf = new BoardListFrame(str);
//			}
//		});
//
//		Button menuBtn2 = new Button("\uACE0\uAC1D\uAD00\uB9AC");
//		menuBar.add(menuBtn2);
//
//		Button menuBtn3 = new Button("\uC608\uC57D\uAD00\uB9AC");
//		
//		menuBtn3.addActionListener(new ActionListener() {
//			public void actionPerformed(ActionEvent e) {
//				CalendarBooking cb = new CalendarBooking();
//			}
//		});
//		menuBar.add(menuBtn3);
//
//		Button menuBtn4 = new Button("\uC11C\uBE44\uC2A4/\uC7AC\uACE0\uAD00\uB9AC");
//		menuBar.add(menuBtn4);
		
		
		
		

		JButton boardBtn = new JButton("\uACF5\uC9C0\uC0AC\uD56D");
		boardBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new BoardListFrame(admin_id);
				dispose();
			}
		});
		boardBtn.setBounds(10, 104, 118, 75);
		contentPane.add(boardBtn);

		JButton customerBtn = new JButton("\uACE0\uAC1D\uAD00\uB9AC");
		customerBtn.setBounds(10, 189, 118, 75);
		contentPane.add(customerBtn);
		
		customerBtn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				dispose();
				CustomerManagePage cm = new CustomerManagePage(admin_id);

			}
		});
		

		JButton bookingBtn = new JButton("\uC608\uC57D\uAD00\uB9AC");
		bookingBtn.setBounds(10, 274, 118, 75);
		contentPane.add(bookingBtn);

		bookingBtn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				dispose();
				BookNow cb = new BookNow(admin_id);

			}
		});
		
		
		JButton btnNewButton_1_2 = new JButton("\uC7AC\uACE0\uAD00\uB9AC");
		btnNewButton_1_2.setBounds(10, 359, 118, 75);
		contentPane.add(btnNewButton_1_2);
		
		JLabel lblNewLabel = new JLabel("New label");
		lblNewLabel.setIcon(new ImageIcon(CustomerManagePage.class.getResource("/swingproject/image/logo2.png")));
		lblNewLabel.setBounds(0, 10, 179, 84);
		contentPane.add(lblNewLabel);
		
		btnNewButton_1_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new StockFrame(admin_id);
				dispose();
			}
		});
		
		
		setVisible(true);
		
		
	}
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CustomerManagePage frame = new CustomerManagePage(admin_id);
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public static String getStr() {
		return str;
	}

	public void setStr(String str) {
		this.str = str;
	}
}
