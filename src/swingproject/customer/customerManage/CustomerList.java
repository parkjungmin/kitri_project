
package swingproject.customer.customerManage;

import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import swingproject.dao.DBConnection;

import java.awt.Font;
import javax.swing.ScrollPaneConstants;
import java.awt.Color;

public class CustomerList extends JPanel {

	// DB에서 스윙 화면으로 테이블 값 가져오기(select) , 저장하기(insert), 수정하기(update), 삭제하기(delete)
	private static final long serialVersionUID = 1L;
	private JButton jBtnAddRow = null; // 테이블 한줄 추가 버튼
	private JButton jBtnSaveRow = null; // 테이블 한줄 저장 버튼
	private JButton jBtnEditRow = null; // 테이블 한줄 저장 버튼
	private JButton jBtnDelRow = null; // 테이블 한줄 삭제 벅튼
	private JTable table;
	private JScrollPane scrollPane; // 테이블 스크롤바 자동으로 생성되게 하기

	private String driver = "oracle.jdbc.driver.OracleDriver";
	private String url = "jdbc:oracle:thin:@192.168.0.24:1521:xe"; // @호스트 IP : 포트 : SID
	private String DB_USER = "c##ora_user";
	private String DB_PASSWORD = "123";
	private String colNames[] = { "고객번호", "이름", "전화번호", "주소", "생년월일", "아이디"}; // 테이블 컬럼 값들
	private DefaultTableModel model = new DefaultTableModel(colNames, 0); // 테이블 데이터 모델 객체 생성

	private Connection con = null;
	private PreparedStatement pstmt = null;
	private ResultSet rs = null; // 리턴받아 사용할 객체 생성 ( select에서 보여줄 때 필요 )
	private String str;

	public CustomerList() {
		setBackground(new Color(245, 255, 250));
		setLayout(null); // 레이아웃 배치관리자 삭제
		setSize(809,414);
		table = new JTable(model);
		table.setFont(new Font("굴림", Font.PLAIN, 13));
		table.addMouseListener(new JTableMouseListener()); // 테이블에 마우스리스너 연결
		scrollPane = new JScrollPane(table); // 테이블에 스크롤 생기게 하기
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane.setLocation(0, 0);
		scrollPane.setSize(809, 370);
		add(scrollPane);
		initialize();
		select();

	}

	public CustomerList(String str) {
		setLayout(null); // 레이아웃 배치관리자 삭제
		setSize(809,414);
		setBackground(new Color(245, 255, 250));

		table = new JTable(model);
		table.setFont(new Font("굴림", Font.PLAIN, 13));
		table.addMouseListener(new JTableMouseListener()); // 테이블에 마우스리스너 연결
		scrollPane = new JScrollPane(table); // 테이블에 스크롤 생기게 하기
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

		scrollPane.setLocation(0, 0);
		scrollPane.setSize(809, 370);
		add(scrollPane);
		initialize();
		this.str = str;
		select(this.str);

	}

	private class JTableMouseListener implements MouseListener { // 마우스로 눌려진값확인하기
		public void mouseClicked(java.awt.event.MouseEvent e) { // 선택된 위치의 값을 출력

			JTable jtable = (JTable) e.getSource();
			int row = jtable.getSelectedRow(); // 선택된 테이블의 행값
			int col = jtable.getSelectedColumn(); // 선택된 테이블의 열값

			System.out.println(model.getValueAt(row, col)); // 선택된 위치의 값을 얻어내서 출력

		}

		public void mouseEntered(java.awt.event.MouseEvent e) {
		}

		public void mouseExited(java.awt.event.MouseEvent e) {
		}

		public void mousePressed(java.awt.event.MouseEvent e) {
		}

		public void mouseReleased(java.awt.event.MouseEvent e) {
		}
	}

	public void select() { // 테이블에 보이기 위해 검색

		String query = "SELECT * FROM CUSTOMER ORDER BY CUSTOMER_NO ASC";
		try {
			Class.forName(driver);
			con = DriverManager.getConnection(url, DB_USER, DB_PASSWORD);
			pstmt = con.prepareStatement(query);
			rs = pstmt.executeQuery(); // 리턴받아와서 데이터를 사용할 객체생성

			while (rs.next()) { // 각각 값을 가져와서 테이블값들을 추가
				model.addRow(new Object[] { rs.getString("CUSTOMER_NO"), rs.getString("CUSTOMER_NAME"),
						rs.getString("CUSTOMER_PHONE"), rs.getString("CUSTOMER_ADDRESS"),
						rs.getString("CUSTOMER_BIRTH"), rs.getString("CUSTOMER_ID")});
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			try {
				rs.close();
				pstmt.close();
				con.close(); // 객체 생성한 반대 순으로 사용한 객체는 닫아준다.
			} catch (Exception e2) {
			}
		}
	}

	private void select(String str) { // 테이블에 보이기 위해 검색

		System.out.println("SELECT * FROM CUSTOMER Where " +

				"CUSTOMER_NO LIKE " + "'%" + str + "%'" + "OR " + "CUSTOMER_NAME LIKE " + "'%" + str + "%'" + "OR "
				+ "CUSTOMER_PHONE LIKE " + "'%" + str + "%'" + "OR " + "CUSTOMER_ADDRESS LIKE " + "'%" + str + "%'"
				+ "OR " + "CUSTOMER_BIRTH LIKE " + "'%" + str + "%'" + "OR " + "CUSTOMER_ID LIKE " + "'%" + str + "%'"
				+ "OR " + "ADMIN_NO LIKE " + "'%" + str + "%'");
		String query = "SELECT * FROM CUSTOMER Where " + "CUSTOMER_NO LIKE " + "'%" + str + "%'" + "OR "
				+ "CUSTOMER_NAME LIKE " + "'%" + str + "%'" + "OR " + "CUSTOMER_PHONE LIKE " + "'%" + str + "%'" + "OR "
				+ "CUSTOMER_ADDRESS LIKE " + "'%" + str + "%'" + "OR " + "CUSTOMER_BIRTH LIKE " + "'%" + str + "%'"
				+ "OR " + "CUSTOMER_ID LIKE " + "'%" + str + "%'" + "OR " + "ADMIN_NO LIKE " + "'%" + str + "%' "
				+ "ORDER BY CUSTOMER_NO ASC";

		try {
			Class.forName(driver);
			con = DriverManager.getConnection(url, DB_USER, DB_PASSWORD);
			pstmt = con.prepareStatement(query);
			rs = pstmt.executeQuery(); // 리턴받아와서 데이터를 사용할 객체생성

			while (rs.next()) { // 각각 값을 가져와서 테이블값들을 추가
				model.addRow(new Object[] { rs.getString("CUSTOMER_NO"), rs.getString("CUSTOMER_NAME"),
						rs.getString("CUSTOMER_PHONE"), rs.getString("CUSTOMER_ADDRESS"),
						rs.getString("CUSTOMER_BIRTH"), rs.getString("CUSTOMER_ID"), rs.getString("ADMIN_NO") });
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			try {
				rs.close();
				pstmt.close();
				con.close(); // 객체 생성한 반대 순으로 사용한 객체는 닫아준다.
			} catch (Exception e2) {
			}
		}
	}

	private void initialize() {

		// 선택된 테이블 한줄 수정하는 부분
		jBtnEditRow = new JButton();
		jBtnEditRow.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				System.out.println(e.getActionCommand()); // 선택된 버튼의 텍스트값 출력
				DefaultTableModel model2 = (DefaultTableModel) table.getModel();
				int row = table.getSelectedRow();
				if (row < 0)
					return; // 선택이 안된 상태면 -1리턴

				String query = "UPDATE CUSTOMER SET CUSTOMER_NAME=?, CUSTOMER_PHONE=?, CUSTOMER_ADDRESS=?, CUSTOMER_BIRTH=?, CUSTOMER_ID=?, ADMIN_NO=? "
						+ "where CUSTOMER_NO=?";

				try {
					Class.forName(driver); // 드라이버 로딩
					con = DriverManager.getConnection(url, DB_USER, DB_PASSWORD); // DB 연결
					pstmt = con.prepareStatement(query);

					// 물음표가 4개 이므로 4개 각각 입력해줘야한다.
					pstmt.setString(1, (String) model2.getValueAt(row, 1));
					pstmt.setString(2, (String) model2.getValueAt(row, 2));
					pstmt.setString(3, (String) model2.getValueAt(row, 3));
					pstmt.setString(4, (String) model2.getValueAt(row, 4));
					pstmt.setString(5, (String) model2.getValueAt(row, 5));
					pstmt.setString(6, (String) model2.getValueAt(row, 6));
					pstmt.setString(7, (String) model2.getValueAt(row, 0));

					int cnt = pstmt.executeUpdate();
					// pstmt.executeUpdate(); create insert update delete
					// pstmt.executeQuery(); select
				} catch (Exception eeee) {
					System.out.println(eeee.getMessage());
				} finally {
					try {
						pstmt.close();
						con.close();
					} catch (Exception e2) {
					}
				}
				model2.setRowCount(0); // 전체 테이블 화면을 지워줌
				if (str.isEmpty()) {
					select(); // 저장 후 다시 전체 값들을 받아옴.
				} else {
					select(str);
				}
			}
		});
		
		// 추가 버튼 설정
		JButton btnNewButton = new JButton("\uCD94\uAC00");
		
		
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AddCustomer ac = new AddCustomer();
				
				ac.addWindowListener(new WindowAdapter() {
					
					@Override
					public void windowClosed(WindowEvent e) {
						model.setRowCount(0); // 전체 테이블 화면을 지워줌
						if (str.isEmpty()) {
							select(); // 저장 후 다시 전체 값들을 받아옴.
						} else {
							select(str);
						}
						
						
					}

					
				});
			}
		});
		
		
		
		
		
		
		btnNewButton.setBounds(363, 380, 140, 23);
		add(btnNewButton);
		jBtnEditRow.setBounds(515, 380, 140, 23);
		jBtnEditRow.setText("수정");
		add(jBtnEditRow);

		// 선택된 테이블 한줄 삭제하는 부분
		jBtnDelRow = new JButton();
		jBtnDelRow.addActionListener(new ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent e) {
				System.out.println(e.getActionCommand()); // 선택된 버튼의 텍스트값 출력
				DefaultTableModel model2 = (DefaultTableModel) table.getModel();
				int row = table.getSelectedRow();
				if (row < 0)
					return; // 선택이 안된 상태면 -1리턴
				String query = "delete from CUSTOMER where CUSTOMER_NO= ?";

				try {
					Class.forName(driver); // 드라이버 로딩
					con = DriverManager.getConnection(url, DB_USER, DB_PASSWORD); // DB 연결
					pstmt = con.prepareStatement(query);

					// 물음표가 1개 이므로 4개 각각 입력해줘야한다.
					pstmt.setString(1, (String) model2.getValueAt(row, 0));
					int cnt = pstmt.executeUpdate();
					// pstmt.executeUpdate(); create insert update delete
					// pstmt.executeQuery(); select
				} catch (Exception eeee) {
					System.out.println(eeee.getMessage());
				} finally {
					try {
						pstmt.close();
						con.close();
					} catch (Exception e2) {
					}
				}
				model2.removeRow(row); // 테이블 상의 한줄 삭제
			}
		});
		jBtnDelRow.setBounds(new Rectangle(667, 380, 140, 23));
		jBtnDelRow.setText("삭제");
		add(jBtnDelRow);
		
		JButton export = new JButton("csv\uD30C\uC77C \uBCC0\uD658");
		export.setBounds(211, 380, 140, 23);
		add(export);
		
		export.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				Date sysdate = new Date();
				SimpleDateFormat format;
				format = new SimpleDateFormat("yyyy년MM월dd일HH시mm분");
				String filePath = "C:\\Users\\Administrator\\Desktop\\csv파일\\고객관리" + format.format(sysdate) + ".csv";
				File file = new File(filePath);



				try {
					//					int rowNo = 0;
					FileWriter fw = new FileWriter(file);
					BufferedWriter bw = new BufferedWriter(fw);


					//컬럼명
					bw.write("고객 데이터");
					bw.newLine();
					bw.newLine();

					for(int k = 0; k < table.getColumnCount(); k++) {
						bw.write(table.getColumnName(k) + ",");

					}
					bw.newLine();
					//셀 내용 채우기
					for(int i = 0; i < table.getRowCount(); i++) {
						for(int j = 0; j <table.getColumnCount(); j++) {
							bw.write(table.getValueAt(i, j).toString());
							bw.write(",");
						}
						bw.newLine();

					}

					bw.close();
					fw.close();
					System.out.println(table.getRowCount());
					System.out.println(table.getSelectedColumn());
					System.out.println(table.getSelectedRow());
					System.out.println(table.getColumnCount());
					JOptionPane.showMessageDialog(null, "변환 성공");
					System.out.println("ggg");
				} catch (IOException ex) {
					ex.printStackTrace();
					JOptionPane.showMessageDialog(null, "변환 실패");
				}
				
			}
		});
		

	}
}


