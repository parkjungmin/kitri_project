package swingproject.customer.customerManage;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ContainerAdapter;
import java.awt.event.ContainerEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import swingproject.dao.MemberDAO;
import swingproject.domain.Member;
import swingproject.view.JoinFrame;
import swingproject.view.LoginFrame;
import javax.swing.SwingConstants;

public class AddCustomer extends JFrame {

	public JButton bt_checkId;
	private JPanel contentPane;
	private JTextField tfCustomer_id;
	private JTextField tfCustomer_pw;
	private JTextField tfCustomer_name;
	private JTextField tfCustomer_phone;
	private JTextField tfCustomer_birth;
	private ActionEvent aeresult;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					JoinFrame frame = new JoinFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public AddCustomer() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 500, 500);
		this.setLocationRelativeTo(null);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblNewLabel = new JLabel("\uACE0\uAC1D\uCD94\uAC00");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setFont(new Font("굴림", Font.PLAIN, 30));
		lblNewLabel.setBounds(172, 30, 159, 49);
		contentPane.add(lblNewLabel);

		tfCustomer_id = new JTextField();
		tfCustomer_id.setBounds(156, 100, 200, 21);
		contentPane.add(tfCustomer_id);
		tfCustomer_id.setColumns(10);

		tfCustomer_pw = new JPasswordField();
		tfCustomer_pw.setBounds(156, 140, 200, 21);
		contentPane.add(tfCustomer_pw);
		tfCustomer_pw.setColumns(10);

		tfCustomer_name = new JTextField();
		tfCustomer_name.setColumns(10);
		tfCustomer_name.setBounds(156, 180, 200, 21);
		contentPane.add(tfCustomer_name);

		tfCustomer_phone = new JTextField();
		tfCustomer_phone.setColumns(10);
		tfCustomer_phone.setBounds(156, 220, 200, 21);
		contentPane.add(tfCustomer_phone);

		tfCustomer_birth = new JTextField();
		tfCustomer_birth.setColumns(10);
		tfCustomer_birth.setBounds(156, 260, 200, 21);
		contentPane.add(tfCustomer_birth);

		JLabel lblNewLabel_1 = new JLabel("id");
		lblNewLabel_1.setBounds(60, 103, 57, 15);
		contentPane.add(lblNewLabel_1);

		JLabel lblNewLabel_2 = new JLabel("password");
		lblNewLabel_2.setBounds(60, 143, 57, 15);
		contentPane.add(lblNewLabel_2);

		JLabel lblNewLabel_3 = new JLabel("\uC774\uB984");
		lblNewLabel_3.setBounds(60, 183, 57, 15);
		contentPane.add(lblNewLabel_3);

		JLabel lblNewLabel_4 = new JLabel("\uC804\uD654\uBC88\uD638");
		lblNewLabel_4.setBounds(60, 223, 57, 15);
		contentPane.add(lblNewLabel_4);

		JLabel lblNewLabel_5 = new JLabel("\uC0DD\uB144\uC6D4\uC77C");
		lblNewLabel_5.setBounds(60, 263, 57, 15);
		contentPane.add(lblNewLabel_5);

		JTextArea taCustomer_address = new JTextArea();
		taCustomer_address.setBounds(156, 312, 200, 57);
		contentPane.add(taCustomer_address);

		JLabel lblNewLabel_6 = new JLabel("\uC8FC\uC18C");
		lblNewLabel_6.setBounds(60, 317, 57, 15);
		contentPane.add(lblNewLabel_6);

		JButton joinCompleteBtn = new JButton("\uD655\uC778");
		joinCompleteBtn.setBounds(156, 396, 200, 55);
		contentPane.add(joinCompleteBtn);

		JButton bt_checkId = new JButton("\uC911\uBCF5\uD655\uC778");
		bt_checkId.setBounds(375, 99, 97, 23);
		contentPane.add(bt_checkId);

		setVisible(true);

		joinCompleteBtn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				Member member = new Member();
				member.setCustomer_id(tfCustomer_id.getText());
				member.setCustomer_pw(tfCustomer_pw.getText());
				member.setCustomer_name(tfCustomer_name.getText());
				member.setCustomer_phone(tfCustomer_phone.getText());
				member.setCustomer_birth(tfCustomer_birth.getText());
				member.setCustomer_address(taCustomer_address.getText());

				MemberDAO dao = MemberDAO.getInstance();
				int result = dao.save(member);

				if (result == 1) {
					JOptionPane.showMessageDialog(null, "고객 추가 완료");
					dispose();

				} else {
					JOptionPane.showMessageDialog(null, "고객 추가 실패");

				}

			}

		});

//		bt_checkId.addActionListener(new ActionListener() {
//			
//			@Override
//			public void actionPerformed(ActionEvent e) {
//				MemberDAO dao = MemberDAO.getInstance();
//				Object ob = e.getSource();
//				if(ob==JoinFrame.bt_checkId) {
//					if(dao.findExistId(JoinFrame.tfCustomer_id.getText())) {
//						JOptionPane.showMessageDialog(null, "사용중인 아이디");
//						JoinFrame.tfCustomer_id.setText("");
//						return;
//					} else {
//						JOptionPane.showMessageDialog(null, "사용 가능합니다");
//					}
//				}
//				
//			}
//		});

		bt_checkId.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				MemberDAO dao = MemberDAO.getInstance();
				Object ob = e.getSource();
				if (tfCustomer_id.getText().length() > 0) {
					if (dao.findExistId(tfCustomer_id.getText())) {
						JOptionPane.showMessageDialog(null, "사용중인 아이디입니다");
						tfCustomer_id.setText("");
						return;
					} else {
						JOptionPane.showMessageDialog(null, "사용 가능한 아이디입니다");
					}
				}

			}
		});
	}

	public ActionEvent getAeresult() {
		return aeresult;
	}

	public void UpdateCustomerList() {
		new CustomerList();
	}

}
