package swingproject.dao;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Vector;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableModel;

import swingproject.customer.customerManage.CustomerManagePage;
import swingproject.view.BoardListFrame;
import swingproject.view.LoginFrame;
import swingproject.view.StockFrame;

public class BookNow extends JFrame{

	private JFrame frame;
	private JPanel wrapper_panel;
	private JTable tableView;
	private StringBuilder sbuilder;
	private JScrollPane scrollList;
	private static String admin_id;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					BookNow window = new BookNow(admin_id);
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public BookNow(String admin_id) {
		initialize(admin_id);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize(String admin_id) {
		frame = new JFrame("예약현황");
		frame.getContentPane().setBackground(new Color(245, 255, 250));
		frame.setBounds(140,190,1000,600);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.setResizable(false);
		frame.getContentPane().setLayout(null);
		
		JLabel adminid = new JLabel(admin_id + "님 환영합니다");
		adminid.setBounds(725, 41, 164, 15);
		frame.getContentPane().add(adminid);
		
		JButton logoutBtn = new JButton("\uB85C\uADF8\uC544\uC6C3");
		logoutBtn.setBounds(861, 37, 88, 23);
		frame.getContentPane().add(logoutBtn);
		JScrollPane selectScrollPane = new JScrollPane();
		selectScrollPane.setBounds(140, 104, 809, 408);
		frame.getContentPane().add(selectScrollPane);
		JTable table = new JTable();
		table.getTableHeader().setReorderingAllowed(false);
		selectScrollPane.setViewportView(table);
		Vector<String> columnNames=new Vector<String>(Arrays.asList("예약번호","예약자명","예약날짜","예약시간","예약서비스","연락처"));
		//제목열 Vector 객체를 DefaultTableModel객체에 추가=> 행번호 0으로 설정
		DefaultTableModel dtm=new DefaultTableModel(columnNames, 0);
		//JTable 에 DefaultTableModel 전달하여 제목 표시
		table.setModel(dtm);
		System.out.println("admin id " + admin_id);

		logoutBtn.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(null, "로그아웃");
				dispose();
				new LoginFrame();
				
			}
		});
		
		
		JButton btnNewButton = new JButton("\uC0AD\uC81C");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//삭제
				String DB_URL = "jdbc:oracle:thin:@192.168.0.24:1521:xe";
				String DB_USER = "c##ora_user";
				String DB_PASSWORD = "123";
				Connection conn = null;
				Statement stmt = null;
				PreparedStatement ps = null;
				ResultSet rs = null;
				String query = "delete from booking where book_no=?";
				DefaultTableModel model = (DefaultTableModel)table.getModel();
				int row = table.getSelectedRow();
				int result=3;
				if(row<0) {
					JOptionPane.showMessageDialog(null, "삭제할 정보를 선택해주세요.");
				}else {
					result = JOptionPane.showConfirmDialog(null, "삭제하시겠습니까?", "예약 삭제 확인창", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);

					if(result==0) {
						if(row<0)return;
						try {
							Class.forName("oracle.jdbc.driver.OracleDriver");
							System.out.println("드라이버 로드 성공");
						}catch(Exception e1) {
							e1.printStackTrace();
						}
						try {
							conn = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);
							System.out.println("DB connect");
							stmt=conn.createStatement();
							ps = conn.prepareStatement(query);
							ps.setInt(1, (int)model.getValueAt(row, 0));
							ps.executeUpdate();
						}catch(Exception e1){

						}finally {
							try {
								ps.close();
								stmt.close();
								conn.close();
							}catch(Exception e1) {

							}
						}
						model.removeRow(row); // 테이블 상의 한줄 삭제
						JOptionPane.showMessageDialog(null, "삭제가 완료되었습니다");
					}else {
						return;
					}
				}
			}
		});
		btnNewButton.setBounds(837, 522, 112, 28);
		frame.getContentPane().add(btnNewButton);

		JButton btnNewButton_1 = new JButton("\uC218\uC815");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//수정
				String DB_URL = "jdbc:oracle:thin:@192.168.0.24:1521:xe";
				String DB_USER = "c##ora_user";
				String DB_PASSWORD = "123";
				Connection conn = null;
				Statement stmt = null;
				PreparedStatement ps = null;
				ResultSet rs = null;
				String query = "update booking set book_name=?, book_date=?, book_time=?, book_content=?, book_phone=? where book_no=?";
				DefaultTableModel model = (DefaultTableModel)table.getModel();
				int row = table.getSelectedRow();
				int result=3;
				if(row<0) {
					JOptionPane.showMessageDialog(null, "수정할 정보를 선택해주세요.");
				}else {
					result = JOptionPane.showConfirmDialog(null, "수정하시겠습니까?", "예약 수정 확인창", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);

					if(result==0) {
						try {
							Class.forName("oracle.jdbc.driver.OracleDriver");
							System.out.println("드라이버 로드 성공");
						}catch(Exception e1) {
							e1.printStackTrace();
						}
						try {
							conn = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);
							System.out.println("DB connect");
							stmt=conn.createStatement();
							ps = conn.prepareStatement(query);
							ps.setInt(6, (int)model.getValueAt(row, 0));
							ps.setString(1, (String)model.getValueAt(row, 1));
							ps.setString(2, (String)model.getValueAt(row, 2));
							ps.setString(3, (String)model.getValueAt(row, 3));
							ps.setString(4, (String)model.getValueAt(row, 4));
							ps.setString(5, (String)model.getValueAt(row, 5));
							ps.executeUpdate();
							System.out.println("수정완료");
						}catch(Exception e1){

						}finally {
							try {
								ps.close();
								stmt.close();
								conn.close();
							}catch(Exception e1) {

							}
						}
						JOptionPane.showMessageDialog(null, "수정이 완료되었습니다");
						model.setRowCount(0);
						try {
							Class.forName("oracle.jdbc.driver.OracleDriver");
							System.out.println("드라이버 로드 성공");

							conn = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);
							System.out.println("디비연결 성공");

							query="select * from booking order by to_date(book_date, 'yyyy/mm/dd') desc, book_time";
							ps=conn.prepareStatement(query);
							rs=ps.executeQuery();
							DefaultTableModel dtm2=(DefaultTableModel)table.getModel();

							while(rs.next()) {
								int book_no = rs.getInt(1);
								String name=rs.getString(2);
								String date=rs.getString(3);
								String time=rs.getString(4);
								String content=rs.getString(5);
								String phone=rs.getString(6);

								//Vector 객체에 1개 레코드데이터 추가
								Vector rowData=new Vector();
								rowData.add(book_no);
								rowData.add(name);
								rowData.add(date);
								rowData.add(time);
								rowData.add(content);
								rowData.add(phone);

								dtm2.addRow(rowData);		//1개레코드 Vector 객체 추가=> 반복

							}

						}catch (ClassNotFoundException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						} catch (Exception e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}finally {
							try {
								ps.close();
								conn.close();
								rs.close();
							} catch (Exception e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}		
						}
					}else {
						return;
					}
				}
			}
		});
		btnNewButton_1.setBounds(713, 522, 112, 28);
		frame.getContentPane().add(btnNewButton_1);

		JButton btnNewButton_2 = new JButton("\uACF5\uC9C0\uC0AC\uD56D");
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.setVisible(false);
				new BoardListFrame(admin_id);
				dispose();
			}
		});
		btnNewButton_2.setBounds(10, 104, 118, 75);
		frame.getContentPane().add(btnNewButton_2);

		JButton btnNewButton_2_1 = new JButton("\uACE0\uAC1D\uAD00\uB9AC");
		btnNewButton_2_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.setVisible(false);
				dispose();
				CustomerManagePage cm = new CustomerManagePage(admin_id);
			}
		});
		btnNewButton_2_1.setBounds(10, 189, 118, 75);
		frame.getContentPane().add(btnNewButton_2_1);

		JButton btnNewButton_2_2 = new JButton("\uC608\uC57D\uAD00\uB9AC");
		btnNewButton_2_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.dispose();

				new BookNow(admin_id);
				
			}
		});
		btnNewButton_2_2.setBounds(10, 274, 118, 75);
		frame.getContentPane().add(btnNewButton_2_2);

		//		JButton btnNewButton_2_3 = new JButton("\uC7AC\uACE0\uAD00\uB9AC");
		//		btnNewButton_2_3.setBounds(0, 359, 128, 75);
		//		frame.getContentPane().add(btnNewButton_2_3);

		JLabel lbTitle = new JLabel("예약 리스트");
		lbTitle.setPreferredSize(new Dimension(738, 50));
		lbTitle.setOpaque(true);
		lbTitle.setHorizontalAlignment(SwingConstants.CENTER);
		lbTitle.setFont(new Font("나눔고딕", Font.BOLD, 20));
		lbTitle.setBackground(new Color(245, 255, 250));
		lbTitle.setBounds(275, 0, 542, 94);
		frame.getContentPane().add(lbTitle);

		JButton export = new JButton("CSV\uD30C\uC77C\uBCC0\uD658");
		export.setBounds(589, 522, 112, 28);
		frame.getContentPane().add(export);
		
		JLabel lblNewLabel = new JLabel(new ImageIcon(LoginFrame.class.getResource("/swingproject/image/logo2.png")));
		lblNewLabel.setBounds(12, 10, 164, 84);
		frame.getContentPane().add(lblNewLabel);
		
		JButton stockBtn = new JButton("\uC7AC\uACE0\uAD00\uB9AC");
		stockBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new StockFrame(admin_id);
				frame.dispose();
			}
		});
		stockBtn.setBounds(10, 359, 118, 75);
		frame.getContentPane().add(stockBtn);

		//새로운 프레임창 열기
		frame.setVisible(true);

		String DB_URL = "jdbc:oracle:thin:@192.168.0.24:1521:xe";
		String DB_USER = "c##ora_user";
		String DB_PASSWORD = "123";

		Connection conn=null;
		PreparedStatement ps=null;
		ResultSet rs=null;
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
			System.out.println("드라이버 로드 성공");

			conn = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);
			
			System.out.println("디비연결 성공");

			String query="select * from booking order by to_date(book_date, 'yyyy/mm/dd') desc, book_time";
			ps=conn.prepareStatement(query);
			rs=ps.executeQuery();
			DefaultTableModel dtm2=(DefaultTableModel)table.getModel();

			while(rs.next()) {
				int book_no = rs.getInt(1);
				String name=rs.getString(2);
				String date=rs.getString(3);
				String time=rs.getString(4);
				String content=rs.getString(5);
				String phone=rs.getString(6);

				//Vector 객체에 1개 레코드데이터 추가
				Vector rowData=new Vector();
				rowData.add(book_no);
				rowData.add(name);
				rowData.add(date);
				rowData.add(time);
				rowData.add(content);
				rowData.add(phone);

				dtm2.addRow(rowData);		//1개레코드 Vector 객체 추가=> 반복

			}

		}catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			try {
				ps.close();
				conn.close();
				rs.close();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}		

		}

		export.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				Date sysdate = new Date();
				SimpleDateFormat format;
				format = new SimpleDateFormat("yyyy년MM월dd일HH시mm분");
				String filePath = "C:\\Users\\Administrator\\Desktop\\csv파일\\예약관리" + format.format(sysdate) + ".csv";
				File file = new File(filePath);



				try {
					//					int rowNo = 0;
					FileWriter fw = new FileWriter(file);
					BufferedWriter bw = new BufferedWriter(fw);


					//컬럼명

					bw.write("예약 데이터");
					bw.newLine();
					bw.newLine();
					
					for(int k = 0; k < table.getColumnCount(); k++) {
						bw.write(table.getColumnName(k) + ",");

					}
					bw.newLine();
					//셀 내용 채우기
					for(int i = 0; i < table.getRowCount(); i++) {
						for(int j = 0; j <table.getColumnCount(); j++) {
							bw.write(table.getValueAt(i, j).toString());
							bw.write(",");
						}
						bw.newLine();

					}

					bw.close();
					fw.close();
					System.out.println(table.getRowCount());
					System.out.println(table.getSelectedColumn());
					System.out.println(table.getSelectedRow());
					System.out.println(table.getColumnCount());
					JOptionPane.showMessageDialog(null, "변환 성공");
					System.out.println("ggg");
				} catch (IOException ex) {
					ex.printStackTrace();
					JOptionPane.showMessageDialog(null, "변환 실패");
				}

			}
		});

	}
}
