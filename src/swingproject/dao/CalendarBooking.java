package swingproject.dao;

import java.awt.*;

import java.awt.event.*;

import java.util.Calendar;

import javax.swing.*;

import swingproject.view.LoginFrame;

public class CalendarBooking extends JFrame implements ActionListener {

	
	// North
	public static String customer_id;
	

	JPanel topPane = new JPanel();

	JButton prevBtn = new JButton("◀");

	JButton nextBtn = new JButton("▶");

	JButton dayBtn = new JButton();

	JLabel yearLbl = new JLabel("년");

	JLabel monthLbl = new JLabel("월");

	JComboBox<Integer> yearCombo = new JComboBox<Integer>();

	DefaultComboBoxModel<Integer> yearModel = new DefaultComboBoxModel<Integer>();

	JComboBox<Integer> monthCombo = new JComboBox<Integer>();

	DefaultComboBoxModel<Integer> monthModel = new DefaultComboBoxModel<Integer>();

	// Center

	JPanel centerPane = new JPanel(new BorderLayout());

	JPanel titlePane = new JPanel(new GridLayout(1, 7));



	String titleStr[] = { "일", "월", "화", "수", "목", "금", "토" };

	JPanel datePane = new JPanel(new GridLayout(0, 7));

	Calendar now;

	int year, month, date;
	public static String cid;
	private final JButton btnNewButton = new JButton("\uB85C\uADF8\uC544\uC6C3");

	public CalendarBooking(String customer_id) {
		getContentPane().setBackground(new Color(0, 0, 0));
		setBackground(new Color(0, 0, 0));
		
		cid=customer_id;
		setBounds(460, 180, 1000, 800);
		now = Calendar.getInstance(); // 현재 날짜

		year = now.get(Calendar.YEAR);

		month = now.get(Calendar.MONTH) + 1;

		date = now.get(Calendar.DATE);
		FlowLayout flowLayout = (FlowLayout) topPane.getLayout();
		topPane.add(prevBtn);

		for (int i = year - 100; i <= year + 50; i++) {

			yearModel.addElement(i);

		}

		yearCombo.setModel(yearModel);

		yearCombo.setSelectedItem(year); // 현재 년도 선택

		topPane.add(yearCombo);
		yearLbl.setForeground(new Color(255, 255, 240));

		topPane.add(yearLbl);

		for (int i = 1; i <= 12; i++) {

			monthModel.addElement(i);

		}

		monthCombo.setModel(monthModel);

		monthCombo.setSelectedItem(month); // 현재 월 선택

		topPane.add(monthCombo);
		monthLbl.setForeground(new Color(255, 255, 240));

		topPane.add(monthLbl);

		topPane.add(nextBtn);

		topPane.setBackground(new Color(245, 255, 250));

		getContentPane().add(topPane, "North");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//로그아웃
				JOptionPane.showMessageDialog(null, "로그아웃");
				dispose();
				new LoginFrame();
			}
		});
		
		topPane.add(btnNewButton);

		// Center




		titlePane.setBackground(new Color(255, 182, 193));

		for (int i = 0; i < titleStr.length; i++) {

			JLabel lbl = new JLabel(titleStr[i], JLabel.CENTER);

			if (i == 0) {

				lbl.setForeground(Color.red);

			} else if (i == 6) {

				lbl.setForeground(Color.blue);

			}

			titlePane.add(lbl);

		}
		centerPane.setBackground(new Color(0, 0, 0));

		centerPane.add(titlePane, "North");
		// 날짜 출력

		dayPrint(year, month);
		datePane.setBackground(new Color(245, 255, 250));

		centerPane.add(datePane, "Center");

		getContentPane().add(centerPane, "Center");


		setSize(941, 565);

		setVisible(true);

		prevBtn.addActionListener(this);

		nextBtn.addActionListener(this);

		yearCombo.addActionListener(this);

		monthCombo.addActionListener(this);

		dayBtn.addActionListener(this);
	}

	// Overriding

	public void actionPerformed(ActionEvent ae) {

		Object obj = ae.getSource();

		if (obj instanceof JButton) {

			JButton eventBtn = (JButton) obj;

			int yy = (Integer) yearCombo.getSelectedItem();

			int mm = (Integer) monthCombo.getSelectedItem();

			if (eventBtn.equals(prevBtn)) { // 전달

				if (mm == 1) {

					yy--;
					mm = 12;

				} else {

					mm--;

				}

			} else if (eventBtn.equals(nextBtn)) { // 다음달

				if (mm == 12) {

					yy++;
					mm = 1;

				} else {

					mm++;

				}

			}

			yearCombo.setSelectedItem(yy);
			year=yy;

			monthCombo.setSelectedItem(mm);
			month=mm;


		}

		else if (obj instanceof JComboBox) { // 콤보박스 이벤트 발생시

			createDayStart();

		}
	}

	public void createDayStart() {

		datePane.setVisible(false); // 패널 숨기기

		datePane.removeAll(); // 날짜 출력한 라벨 지우기

		dayPrint((Integer) yearCombo.getSelectedItem(), (Integer) monthCombo.getSelectedItem());

		datePane.setVisible(true); // 패널 재출력

	}

	public void dayPrint(int y, int m) {

		Calendar cal = Calendar.getInstance();

		cal.set(y, m - 1, 1); // 출력할 첫날의 객체 만든다.

		int week = cal.get(Calendar.DAY_OF_WEEK); // 1일에 대한 요일 일요일 : 0

		int lastDate = cal.getActualMaximum(Calendar.DAY_OF_MONTH); // 그 달의 마지막 날

		for (int i = 1; i < week; i++) { // 날짜 출력 전까지의 공백 출력
			datePane.add(new JLabel(" "));

		}

		for (int i = 1; i <= lastDate; i++) {

			JButton dayBtn = new JButton(String.valueOf(i));

			cal.set(y, m - 1, i);

			int outWeek = cal.get(Calendar.DAY_OF_WEEK);

			if (outWeek == 1) {

				dayBtn.setForeground(Color.red);

			} else if (outWeek == 7) {

				dayBtn.setForeground(Color.BLUE);

			}

			datePane.add(dayBtn);
			dayBtn.addActionListener(new MyActionListener());

		}
		year=y;
		month=m;
	}
	class MyActionListener implements ActionListener{

		//@Override
		public void actionPerformed(ActionEvent e) {
			//날짜 클릭시
			//JOptionPane.showConfirmDialog(null, "예약하시겠습니까?");
//			int result = 0;
//			result = JOptionPane.showConfirmDialog(null, "예약하시겠습니까?", "제목표시줄", JOptionPane.OK_CANCEL_OPTION);
			//System.out.println(e.getActionCommand()+"버튼 클릭");
			String date =year+"/"+month+"/"+e.getActionCommand();
			//System.out.println(date);
			if(e.getActionCommand()!=null) {
				new BookingOn(date, cid);
			}
//			if(result==0) {
//				System.out.println("예약");
//			}else {
//				System.out.println("예약취소");
//			}
			
			
		}

	}
	class BookingOn extends JFrame{
		public BookingOn(String date, String customer_id) {
			EventQueue.invokeLater(new Runnable() {
				public void run() {
					try {
						Booking window = new Booking(date, customer_id);
						window.frame.setVisible(true);
					} catch (Exception e) {
						
						e.printStackTrace();
					}
				}
			});
		}
	}
	public static void main(String[] args) {
		new CalendarBooking(customer_id);

	}
}