package swingproject.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import swingproject.domain.Stock;

public class StockDao {
	public static final String DRIVER_NAME = "oracle.jdbc.OracleDriver";
	public static final String URL = "jdbc:oracle:thin:@192.168.0.24:1521:xe";

	public static final String USER_ID = "c##ora_user";
	public static final String USER_PW = "123";

	Connection conn = null;
	ResultSet rs = null;
	PreparedStatement psmt = null;
	Statement stmt = null;


	
	private static StockDao instance = new StockDao();
	public static StockDao getInstance() {
		return instance;
	}
	
	public int updateAQty(Stock stock) {
		conn = DBConnection.getConnection();
	
		try {
			psmt = conn.prepareStatement("UPDATE STOCK SET stock_qty = stock_qty-1 WHERE STOCK_ID = 1");
			
			psmt.executeQuery();
		} catch(SQLException e) {
			e.printStackTrace();
		}finally {
			try {
				psmt.close();
				conn.close();
				rs.close();
			} catch (Exception e2) {
			}
		}
		return 0;
	
	}
	
	public int updateBQty(Stock stock) {
		conn = DBConnection.getConnection();
	
		try {
			psmt = conn.prepareStatement("UPDATE STOCK SET stock_qty = stock_qty-1 WHERE STOCK_ID = 2");
			
			psmt.executeQuery();
		} catch(SQLException e) {
			e.printStackTrace();
		}finally {
			try {
				psmt.close();
				conn.close();
				rs.close();
			} catch (Exception e2) {
			}
		}
		return 0;
	
	}
	public int updateCQty(Stock stock) {
		conn = DBConnection.getConnection();
	
		try {
			psmt = conn.prepareStatement("UPDATE STOCK SET stock_qty = stock_qty-1 WHERE STOCK_ID = 3");
			
			psmt.executeQuery();
		} catch(SQLException e) {
			e.printStackTrace();
		}finally {
			try {
				psmt.close();
				conn.close();
				rs.close();
			} catch (Exception e2) {
			}
		}
		return 0;
	
	}
	
	public List<Stock> readAll() {
		String sql = "SELECT * FROM STOCK WHERE stock_id <= 3";
		
		conn = DBConnection.getConnection();
		Statement stmt = null;
		ResultSet rs = null;
		List<Stock> lists = new ArrayList<>();
		
		try {
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			
			while(rs.next()) {
				Stock stock = new Stock();
				
				stock = new Stock();
				
				stock.setStock_id(rs.getInt(1));
				stock.setStock_name(rs.getString(2));
				stock.setStock_qty(rs.getInt(3));
				stock.setStock_price(rs.getInt(4));
				
				lists.add(stock);
			}
		} catch(SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				psmt.close();
				conn.close();
				rs.close();
			} catch (Exception e2) {
			}
		}
		return lists;
	}
	
	public int updateAStock(Stock stock) {
		conn = DBConnection.getConnection();
		
		try {
			psmt = conn.prepareStatement("UPDATE stock SET stock_qty = stock_qty + (SELECT ? FROM stock where rownum <= 1) where stock_id = 1");
			
			psmt.setInt(1, stock.getStock_come());
			
			psmt.executeQuery();
		} catch (SQLException e ) {
			e.printStackTrace();
		}finally {
			try {
				psmt.close();
				conn.close();
				rs.close();
			} catch (Exception e2) {
			}
		}
	return 0;
	}
	
	public int updateBStock(Stock stock) {
		conn = DBConnection.getConnection();
		
		try {
			psmt = conn.prepareStatement("UPDATE stock SET stock_qty = stock_qty + (SELECT ? FROM stock where rownum <= 1) where stock_id = 2");
			
			psmt.setInt(1, stock.getStock_come());
			
			psmt.executeQuery();
		} catch (SQLException e ) {
			e.printStackTrace();
		}finally {
			try {
				psmt.close();
				conn.close();
				rs.close();
			} catch (Exception e2) {
			}
		}
	return 0;
	}
	
	public int updateCStock(Stock stock) {
		conn = DBConnection.getConnection();
		
		try {
			psmt = conn.prepareStatement("UPDATE stock SET stock_qty = stock_qty + (SELECT ? FROM stock where rownum <= 1) where stock_id = 3");
			
			psmt.setInt(1, stock.getStock_come());
			
			psmt.executeQuery();
		} catch (SQLException e ) {
			e.printStackTrace();
		}finally {
			try {
				psmt.close();
				conn.close();
				rs.close();
			} catch (Exception e2) {
			}
		}
	return 0;
	}
	
	

}
