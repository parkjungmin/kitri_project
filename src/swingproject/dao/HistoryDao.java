package swingproject.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import swingproject.domain.History;
import swingproject.domain.Stock;

public class HistoryDao {
	public static final String DRIVER_NAME = "oracle.jdbc.OracleDriver";
	public static final String URL = "jdbc:oracle:thin:@192.168.0.24:1521:xe";

	public static final String USER_ID = "c##ora_user";
	public static final String USER_PW = "123";

	Connection conn = null;
	ResultSet rs = null;
	PreparedStatement psmt = null;
	Statement stmt = null;


	
	private static HistoryDao instance = new HistoryDao();
	public static HistoryDao getInstance() {
		return instance;
	}
	
//	public List<History> readAll() {
	public List<Stock> readAll() {
//		String sql = "SELECT history_no, history_name, history_date, history_qty FROM history ORDER BY history_no desc";
		String sql = "SELECT stock_id, stock_name, stock_date, stock_come FROM STOCK WHERE stock_id > 3 ORDER BY stock_id desc";
		
		
		conn = DBConnection.getConnection();
		Statement stmt = null;
		ResultSet rs = null;
//		List<History> lists = new ArrayList<>();
		List<Stock> lists = new ArrayList<>();
		
		try {
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			
			while(rs.next()) {
//				History history = new History();
//				
//				history = new History();
//				
//				history.setHistory_no(rs.getInt(1));
//				history.setHistory_name(rs.getString(2));
//				history.setHistory_date(rs.getDate(3));
//				history.setHistory_qty(rs.getInt(4));
//				
//				lists.add(history);
				Stock stock = new Stock();
				
				stock = new Stock();
				
				stock.setStock_id(rs.getInt(1));
				stock.setStock_name(rs.getString(2));
				stock.setStock_date(rs.getDate(3));
				stock.setStock_come(rs.getInt(4));
				
				lists.add(stock);
//				stock.setStock_come(rs.getCome(4));
			} 
			
		}catch(SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				psmt.close();
				conn.close();
				rs.close();
			} catch (Exception e2) {
			}
		}
		
		return lists;
	}
//	public int orderSave(History history) {
//		conn = DBConnection.getConnection();
//		
//		
//		try {
//			psmt = conn.prepareStatement("INSERT INTO history (history_name, history_qty, history_date, history_no) "
//					+ "VALUES( ?, ?, sysdate, history_seq.nextval)");
//			System.out.println("오더 확인 했어");
////			psmt.setInt(1, history.getHistory_stock_id());
//			psmt.setString(1, history.getHistory_name());
//			psmt.setInt(2, history.getHistory_qty());
//			
//			psmt.executeUpdate();
//			return 1;
//		} catch(Exception e) {
//			e.printStackTrace();
//		}
//	return -1;
//	}
	
	public int orderSave(Stock stock) {
		conn = DBConnection.getConnection();
		
		
		try {
			psmt = conn.prepareStatement("INSERT INTO stock (stock_id, stock_name, stock_date, stock_come ) "
					+ "VALUES( stock_seq.nextval , ?, sysdate, ?)");
			System.out.println("오더 확인 했어");
//			psmt.setInt(1, history.getHistory_stock_id());
			psmt.setString(1, stock.getStock_name());
			psmt.setInt(2, stock.getStock_come());
			
			psmt.executeUpdate();
			return 1;
		} catch(Exception e) {
			e.printStackTrace();
		}finally {
			try {
				psmt.close();
				conn.close();
				rs.close();
			} catch (Exception e2) {
			}
		}
	return -1;
	}
	
	
	
}
