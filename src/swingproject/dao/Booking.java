package swingproject.dao;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.JComboBox;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableModel;

import swingproject.domain.Stock;

public class Booking<E> {
	JFrame frame;

	/**
	 * Launch the application.
	 */
	Stock stock;
	String timeSelect;
	String book_time=null;
	Vector<Object> data = new Vector<Object>();
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Booking window = new Booking();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Booking(){
		initialize(null, null);
	};
	public Booking(String date, String customer_id) {
		System.out.println(customer_id);
		initialize(date, customer_id);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize(String date, String customer_id) {
		DefaultTableModel model = new DefaultTableModel(new String[] {"time"}, 0);
		JTable jTable = new JTable(model);
		String DB_URL = "jdbc:oracle:thin:@192.168.0.24:1521:xe";
		String DB_USER = "c##ora_user";
		String DB_PASSWORD = "123";
		Connection conn = null;
		Statement stmt = null;
		PreparedStatement ps = null;
		ResultSet rs=null;
		Vector<Object> data = new Vector<Object>();
		String sql = "select book_time from booking where book_date=?";
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
			System.out.println("드라이버 로드 성공");
		}catch(Exception e) {
			e.printStackTrace();
		}
		try {
			conn = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);
			System.out.println("DB connect");
			stmt=conn.createStatement();
			ps = conn.prepareStatement(sql);
			ps.setString(1, date);
			rs=ps.executeQuery();
			DefaultTableModel dtm = (DefaultTableModel)jTable.getModel();
			while(rs.next()) {
				book_time=rs.getString(1);
				Vector<Object> row = new Vector<Object>();
				row.add(book_time);
				data.add(row);
			}
			jTable.setModel(dtm);
		}catch(Exception e){

		}finally {
			try {
				ps.close();
				stmt.close();
				conn.close();
			}catch(Exception e) {

			}
		}
		DefaultTableModel time_model = new DefaultTableModel(data, 0);
		frame = new JFrame();
		frame.getContentPane().setBackground(new Color(245, 255, 250));
		frame.setBounds(650, 280, 450, 300);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		JComboBox comboBox = new JComboBox();
		comboBox.addItem("시간 선택");
		comboBox.addItem("11:00");
		comboBox.addItem("12:00");
		comboBox.addItem("13:00");
		comboBox.addItem("15:00");
		comboBox.addItem("16:00");
		comboBox.addItem("17:00");
		comboBox.addItem("18:00");
		comboBox.addItem("19:00");
		for(int i=0;i<data.size();i++) {
			String tmp=data.elementAt(i).toString();
			String removetime=tmp.substring(1, 6);
			comboBox.removeItem(removetime);
		}




		comboBox.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				//시간선택
				Object a= e.getItem();
				timeSelect = a.toString();
			}
		});
		comboBox.setBounds(200, 117, 140, 23);
		frame.getContentPane().add(comboBox);

		JButton btnNewButton = new JButton("\uD655\uC778");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//확인버튼
				System.out.println(timeSelect);
				int result = 3;
				if(timeSelect!=null||timeSelect=="시간선택") {
					Object[] options= {"커트", "펌", "염색"};
					result = JOptionPane.showOptionDialog(null, "예약하고자 하는 서비스를 선택해주세요.", "예약상세내용확인", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, "펌");
					String DB_URL = "jdbc:oracle:thin:@192.168.0.24:1521:xe";
					String DB_USER = "c##ora_user";
					String DB_PASSWORD = "123";

					Connection conn = null;
					Statement stmt = null;
					PreparedStatement ps = null;
					String query = "insert into booking values(book_no_increment.nextval, (select customer_name from customer where customer_id=?), ?, ?, ?, (select customer_phone from customer where customer_id=?),1)";
					try {
						Class.forName("oracle.jdbc.driver.OracleDriver");
						System.out.println("driver");
					}catch(Exception e1) {
						e1.printStackTrace();
					}

					if(result==0) {//커트
						StockDao sdao = new StockDao();
						sdao.updateAQty(stock);
						try {
							
							conn = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);
							stmt=conn.createStatement();
							ps = conn.prepareStatement(query);
							ps.setString(1, customer_id);
							ps.setString(2, date);
							ps.setString(3, (String)comboBox.getSelectedItem());
							ps.setString(4, "커트");
							ps.setString(5, customer_id);
							
							ps.executeUpdate();
							System.out.println(ps);
						}catch(Exception e1){

						}finally {
							try {
								ps.close();
								stmt.close();
								conn.close();
								frame.setVisible(false);
							}catch(Exception e1) {

							}
						}
					}else if(result==1){//펌
						StockDao sdao = new StockDao();
						sdao.updateBQty(stock);
						try {
							
							conn = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);
							stmt=conn.createStatement();
							ps = conn.prepareStatement(query);
							ps.setString(1, customer_id);
							ps.setString(2, date);
							ps.setString(3, (String)comboBox.getSelectedItem());
							ps.setString(4, "펌");
							ps.setString(5, customer_id);
							
							ps.executeUpdate();
							System.out.println(ps);
						}catch(Exception e1){

						}finally {
							try {
								ps.close();
								stmt.close();
								conn.close();
								frame.setVisible(false);
							}catch(Exception e1) {

							}
						}
					}else if(result==2) {//염색
						StockDao sdao = new StockDao();;
						sdao.updateCQty(stock);
						try {
							conn = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);
							stmt=conn.createStatement();
							ps = conn.prepareStatement(query);
							ps.setString(1, customer_id);
							ps.setString(2, date);
							ps.setString(3, (String)comboBox.getSelectedItem());
							ps.setString(4, "염색");
							ps.setString(5, customer_id);
							ps.executeUpdate();
							System.out.println(ps);
						}
						catch(Exception e1){

						}finally {
							try {
								ps.close();
								stmt.close();
								conn.close();
								frame.setVisible(false);
							}catch(Exception e1) {

							}
						}
					}
				}else {
					JOptionPane.showMessageDialog(null, "시간을 선택해주십시오.");
				}
				switch(result) {
				case 0:
					JOptionPane.showMessageDialog(null, date+" "+(String)comboBox.getSelectedItem()+" 커트 \n예약이 완료되었습니다");
					break;
				case 1:
					JOptionPane.showMessageDialog(null, date+" "+(String)comboBox.getSelectedItem()+" 펌\n예약이 완료되었습니다");
					break;
				case 2:
					JOptionPane.showMessageDialog(null, date+" "+(String)comboBox.getSelectedItem()+" 염색\n예약이 완료되었습니다");
					break;
				}
			}
		});
		btnNewButton.setBounds(115, 196, 97, 23);
		frame.getContentPane().add(btnNewButton);

		JButton btnNewButton_1 = new JButton("\uCDE8\uC18C");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//취소버튼
				frame.setVisible(false);
			}
		});
		btnNewButton_1.setBounds(243, 196, 97, 23);
		frame.getContentPane().add(btnNewButton_1);

		JLabel lblNewLabel_1 = new JLabel("\uC2DC\uAC04 \uC120\uD0DD");
		lblNewLabel_1.setBounds(113, 121, 57, 15);
		frame.getContentPane().add(lblNewLabel_1);
		
		JLabel lblNewLabel = new JLabel(new ImageIcon("C:\\Users\\KITRI\\git\\kitri_project\\logo.png"));
		lblNewLabel.setBounds(12, 10, 113, 79);
		frame.getContentPane().add(lblNewLabel);
	}
}
